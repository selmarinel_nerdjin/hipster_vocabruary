@extends('layouts.landing')

@section('title')
    @if($word)
        SlangShot::{{$word}}
    @else
        SlangShot::Word
    @endif
@endsection

@section('meta')
    <meta id="og_url" property="og:url" content="{{route("home",["word" => $word])}}"/>
    <meta id="og_type" property="og:type" content="website"/>
    <meta id="og_title" property="og:title" content="{{$wordModel->name}}"/>
    <meta id="og_description" property="og:description" content="{{$wordModel->description}}"/>
    <meta name="description" content="{{$wordModel->description}}"/>
    <meta name="keywords" content=""/>
    <meta name="author" content="{{$wordModel->getAuthor()->name}}"/>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Exo+2:100" rel="stylesheet">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
    <style>
        body {
            background-color: #f0f0f0 !important;
            padding-bottom: 65px;
        }

        .md-card-footer {
            background-color: #353b41;
            margin: 0 -15px;
        }

        .md-action {
            color: #353b41 !important;
            background-color: #f0f0f0 !important;
        }

        .md-action:hover {
            color: #f0f0f0 !important;
            background-color: #34495e !important;
        }

        .md-action:hover i {
            color: aliceblue !important;
        }

        .md-delete {
            color: #353b41 !important;
            background-color: #e74c3c !important;
        }

        .md-activate {
            background-color: #3498db !important;
            color: #353b41 !important;
        }

        #quill-container {
            height: 300px !important;
        }

        .ql-editor {
            height: 300px !important;
        }

        .quillWrapper {
            height: auto !important;
        }

        .md-app-content {
            background-color: #f0f0f0 !important;
        }

        .social-icon {
            padding: 10px 20px 10px 10px;
            text-align: center;
            border-right: solid 1px;
        }

        .social-text {
            padding: 10px;
        }

        .alphabet {
            padding: 10px 0;
            margin-bottom: -5px;
            font-family: 'Exo 2', sans-serif;
            font-weight: bolder;
            font-size: 24px;
            text-align: center;
            /*background-color: #353b41;*/
            /*color: #fff;*/
        }

        .letter {
            line-height: 36px;
            padding: 5px;
            transition: .5s;
            cursor: pointer;
            text-decoration: none;
            font-style: normal;
        }

        .letter:hover {
            background-color: #343a40;
            color: #eee;
        }

        .letter.active {
            background-color: #34495e;
            color: #eee;
        }

        .letter.active:hover {
            background-color: #3498db;
            color: #eee;
        }

        .title {
            color: #bbb !important;
            font-family: 'Exo 2', sans-serif;
            font-weight: bolder !important;
        }

        .word {
            font-family: 'Exo 2', sans-serif;
            display: block;
            padding: 7px 5px;
            margin: 0 -8px;
        }

        .word:hover {
            background-color: #34495e;
        }

        .word:hover .name {
            color: #ecf0f1;
        }

        .word .title {
            width: 100%;
        }

        .word .name {
            font-size: 18px;
            font-weight: bolder;
            color: #000;
        }

        .word .date {
            font-size: 12px;
            color: #2ecc71;
            font-weight: bolder;
            text-align: right;
        }

        .word.active .date {
            color: #f1c40f;
        }

        .word.active {
            background-color: #3498db;
            color: #eee;
        }

        .footer {
            position: absolute;
            left: 0;
            bottom: 0;
        }

        .ltcard {
            background-color: #fafafa;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .md-app {
            min-height: 100vh;
        }

        .md-drawer {
            width: 100vh;
            max-width: calc(100vw - 125px);
        }

        .md-empty-state {
            transition: none !important;
        }

        .md-menu-content-container {
            background-color: #fefefe;
        }

        .logged {
            color: white;
            font-family: 'Exo 2', sans-serif;
        }

        .white {
            background-color: #fefefe;
        }

        .md-card-login {
            width: 320px;
            margin: 4px;
            display: inline-block;
            vertical-align: top;
        }

        .md-drawer.md-active {
            height: 100vh !important;
            position: fixed !important;
        }

        .list-item-text * {
            width: auto !important;
            text-align: right;
        }

        .underline {
            border-bottom: solid 2px #34495e;
            padding-bottom: 10px !important;
            font-family: "Roboto", "Helvetica", "Arial", sans-serif;
            font-size: 3.5em;
            line-height: 1.5em;
        }

        .exo {
            font-family: "Exo 2", sans-serif;
        }

        .bolder {
            font-weight: bolder !important;
        }

        .roboto {
            font-family: "Roboto", "Helvetica", "Arial", sans-serif;
        }

        .search-toggle {
            position: absolute;
            z-index: 3;
            right: 0;
            opacity: .3;
            transition: .2s;
        }

        .search-toggle:hover {
            opacity: 1;
        }

        .liked {
            color: #e74c3c !important;
        }

        .card {
            width: 150px;
            margin: 5px;
            display: inline-block;
            vertical-align: top;
        }
    </style>
@endsection

@section('content')
    <div class="page-container">
        <md-app>
            <md-app-toolbar class="navbar navbar-expand-lg navbar-dark bg-dark">
                <md-button class="md-icon-button" @click="toggleMenu" v-if="!menuVisible">
                    <md-icon>menu</md-icon>
                </md-button>
                <h3 class="md-title title" style="flex: 1">Slangshot</h3>
                @if(Auth::guest())
                    <button
                            @click="openLoginDialog()"
                            type="button"
                            class="btn btn-outline-light">
                        @lang('home.login')
                    </button>
                @else
                    <div class="logged">
                        <md-menu md-direction="bottom-end" md-size="small">
                            <div md-menu-trigger>
                                <md-avatar>
                                    <img src="{{Auth::user()->getCover()}}" width="30" height="30">
                                </md-avatar>
                                Welcome again, {{ Auth::user()->name }}. &nbsp;
                            </div>
                            <md-menu-content>
                                <md-menu-item
                                        @click="openFilters"
                                        v-if="user.permissions[md5('can_create_filter')]">
                                    <md-icon>label</md-icon>
                                    <span>Configure filters</span>
                                </md-menu-item>
                                <md-menu-item
                                        @click="goUserAdmin"
                                        v-if="user.permissions[md5('can_admin_user')]">
                                    <md-icon>supervisor_account</md-icon>
                                    <span>Administrate users</span>
                                </md-menu-item>
                                <md-divider></md-divider>
                                <md-menu-item @click="getUserInformationToEdit({{Auth::user()->id}})">
                                    <md-icon>settings</md-icon>
                                    <span>Settings</span>
                                </md-menu-item>
                                <md-menu-item @click="getUserInformation({{Auth::user()->id}})">
                                    <md-icon>info</md-icon>
                                    <span>@lang("home.about_self")</span>
                                </md-menu-item>
                                <md-menu-item @click="logout">
                                    <md-icon>exit_to_app</md-icon>
                                    <span>@lang("home.logout")</span>
                                </md-menu-item>
                            </md-menu-content>
                        </md-menu>
                    </div>
                @endif
            </md-app-toolbar>
            @include("v2.components.left_list")
            <md-app-content>
                @include("v2.components.alphabet")
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-6 col-sm-12">
                        <section class="search-toggle">
                            <md-button
                                    @click="showSearchPanel = !showSearchPanel"
                                    class="md-icon-button md-raised md-primary">
                                <md-icon>search</md-icon>
                                <md-tooltip v-show="!showSearchPanel">
                                    open search panel
                                </md-tooltip>
                                <md-tooltip v-show="showSearchPanel">
                                    close search panel
                                </md-tooltip>
                            </md-button>
                        </section>
                        <transition name="fade" mode="out-in" appear>
                            <div class="row" v-show="showSearchPanel">
                                <md-card class="col ltcard">
                                    <md-card-header></md-card-header>
                                    <md-card-content>
                                        <md-field md-clearable>
                                            <md-icon>search</md-icon>
                                            <label>@lang("home.search")</label>
                                            <md-input v-model="query" maxlength="30"></md-input>
                                        </md-field>
                                        <md-button
                                                class="md-raised md-primary btn btn-dark"
                                                @click="find">
                                            @lang("home.search")
                                        </md-button>
                                    </md-card-content>
                                </md-card>
                            </div>
                        </transition>

                        <div class="row" v-if="!word.id">
                            <md-card class="col ltcard">
                                <md-card-content>
                                    <md-empty-state
                                            md-icon="search"
                                            md-label="@lang("home.search")"
                                            md-description="Wait while saving">
                                    </md-empty-state>
                                </md-card-content>
                            </md-card>
                        </div>
                        <div class="row" v-if="word.id">
                            <md-card class="col ltcard">
                                <md-card-header>
                                    <h2>@{{ word.name }}</h2>
                                </md-card-header>
                                <md-card-content v-html="word.description"></md-card-content>
                                <md-card-content v-if="word.filters && filterCount">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="filters">
                                        <md-chip class="md-primary"
                                                 v-for="filter in word.filters">
                                            @{{ filter.name }}
                                        </md-chip>
                                    </div>
                                </md-card-content>
                                <md-card-actions md-alignment="left">
                                    <div class="card-reservation">
                                        <md-icon>favorite</md-icon>
                                        @{{ word.likes.length }}
                                        <md-icon>chat</md-icon>
                                        @{{ word.comments.length }}
                                        <md-icon>link</md-icon>
                                        @{{ word.tags.length }}
                                    </div>
                                </md-card-actions>
                                <md-card-actions md-alignment="space-between">
                                    <div>
                                        @lang('home.by') <b class="user_button"
                                                            @click="getUserInformation(word.author.id)">
                                            @{{ word.author.name }}
                                        </b>
                                        @lang('home.at') @{{ processTime(word.created_at) }}
                                    </div>
                                    <div>
                                        <md-button class="md-icon-button" @click="like(word)">
                                            <md-icon v-bind:class="{liked:word.like}">
                                                favorite
                                            </md-icon>
                                            <div style="
                                                position:absolute;
                                                z-index: 3;
                                                right: -3px;
                                                bottom: -3px"
                                                 class="badge badge-secondary">
                                                @{{ word.likes.length }}
                                            </div>
                                        </md-button>
                                        {{--refact--}}
                                        <social-sharing :url="'{{route("home")}}/'+word.name" inline-template>
                                            <network network="facebook">
                                                <md-button class="md-raised">
                                                    <md-icon>
                                                        share
                                                    </md-icon>
                                                    Share with facebook
                                                </md-button>
                                            </network>
                                        </social-sharing>
                                    </div>
                                </md-card-actions>
                                <md-card-actions
                                        class="md-card-footer"
                                        v-if="user.permissions[md5('can_edit_other_word')] || isOwner">
                                    <md-button class="md-icon-button md-mini"
                                               @click="toggle(word)"
                                               v-if="user.permissions[md5('can_verify_word')] && word.id"
                                               :class="(word.status != 1)? 'md-activate':'md-delete'">
                                        <md-icon>
                                            @{{ (word.status != 1)?'visibility':'visibility_off' }}
                                        </md-icon>
                                    </md-button>

                                    <md-button class="md-icon-button md-mini md-fab md-action"
                                               @click="editWordDialog">
                                        <md-icon>create</md-icon>
                                    </md-button>
                                </md-card-actions>
                            </md-card>
                        </div>
                        <div class="row" v-if="word.id">
                            <md-card class="col ltcard">
                                <md-card-content>
                                    @if(!Auth::guest())
                                        <form novalidate class="md-layout-row md-gutter"
                                              @submit.stop.prevent="commentAction">
                                            <md-field>
                                                <label>@lang('home.comment')</label>
                                                <md-textarea maxlength="255" v-model="comment"></md-textarea>
                                            </md-field>
                                            <div class="text-right" v-show="comment">
                                                <md-button class="md-raised md-mini" type="commentAction">
                                                    <span>@lang('home.send')</span>
                                                    <md-icon>send</md-icon>
                                                </md-button>
                                            </div>
                                        </form>
                                    @else
                                        <md-empty-state
                                                md-icon="fingerprint"
                                                md-label="@lang('home.comments')"
                                                md-description="@lang('home.notice_register')">
                                            <md-button class="md-raised  md-primary btn btn-dark"
                                                       @click="openLoginDialog()">@lang('home.login')</md-button>
                                        </md-empty-state>
                                    @endif
                                </md-card-content>
                                <md-card-content>
                                    <div class="text-center" v-if="loadingComments">
                                        <md-progress-spinner
                                                class="md-accent"
                                                md-mode="indeterminate">
                                        </md-progress-spinner>
                                    </div>
                                    <md-empty-state
                                            md-icon="chat_bubble_outline"
                                            md-label="There is no comments."
                                            md-description="Be first!"
                                            v-if="!word.comments.length && !loadingComments">
                                    </md-empty-state>
                                    <div class="list-group">
                                        <div class="list-group-item list-group-item-action flex-column align-items-start"
                                             v-for="item,key in word.comments">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">
                                                    <md-avatar>
                                                        <img :src="item.user.cover" :alt="item.user.name">
                                                    </md-avatar>
                                                    @{{ item.user.name }}
                                                </h5>
                                                <small>@{{ processTime(item.created_at) }}</small>
                                            </div>
                                            <small v-html="item.content"></small>
                                        </div>
                                    </div>
                                </md-card-content>
                            </md-card>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 col-sm-12">
                        @include("v2.components.synonims")
                        @include("v2.components.popular")
                        @include("v2.components.newest")
                    </div>

                </div>
            </md-app-content>
        </md-app>
    </div>
    <footer class="container-fluid footer">
        <div class="row">
            <div class="col-12 text-center">
                <span class="logo">SlangShot</span>
                &copy; Selmarinel {{ date("Y") }}
            </div>
        </div>
    </footer>
    @include("v2.components.login")
    @include("v2.components.error")
    @include("v2.components.user_info")
    @include("v2.components.word_editor")
    @include("v2.components.user_edit")
    <md-dialog :md-active.sync="showFiltersDialog" style="width: 99%">
        <md-dialog-title>Filters</md-dialog-title>
        <md-dialog-content>
            <md-content class="md-scrollbar text-center">
                <md-card class="md-elevation-4 card"
                         v-for="filter in wordTags">
                    <md-card-header class="text-left">
                        <h5 class="exo">@{{ filter.name }}</h5>
                    </md-card-header>
                    <md-card-actions>
                        <md-button class="md-icon-button md-raised md-activate"
                                   @click="openFilter(filter)">
                            <md-icon>edit</md-icon>
                        </md-button>
                        <md-button class="md-icon-button md-raised md-delete">
                            <md-icon>delete</md-icon>
                        </md-button>
                    </md-card-actions>
                </md-card>
                <md-card class="md-elevation-4 card">
                    <md-card-header>
                        <h5 class="exo">Add Filter</h5>
                    </md-card-header>
                    <md-card-content>
                        <md-button class="md-icon-button md-dense"  @click="openFilterToCreate">
                            <md-icon class="md-size-2x">add_box</md-icon>
                        </md-button>
                    </md-card-content>
                </md-card>
            </md-content>
        </md-dialog-content>
        <md-dialog-actions>
            <md-button @click="showFiltersDialog = false">Close</md-button>
        </md-dialog-actions>
    </md-dialog>
    <md-dialog :md-active.sync="filterDialog.open" :md-click-outside-to-close=false>
        <md-dialog-title>
            @{{ filterDialog.title }}
        </md-dialog-title>
        <md-dialog-content>
            <form>
                <md-field>
                    <label>Filter Name</label>
                    <md-input v-model="filterDialog.item.name"></md-input>
                </md-field>
            </form>
        </md-dialog-content>
        <md-dialog-actions>
            <md-button @click="noSaveFilter">
                Cancel
            </md-button>
            <md-button @click="editFilter" v-if="filterDialog.item.id">
                Save
            </md-button>
            <md-button @click="createFilter" v-if="!filterDialog.item.id">
                Create
            </md-button>
        </md-dialog-actions>
    </md-dialog>
@endsection

@section('scripts')
    <script>
        $(document).ready(function (event) {
            window.addEventListener("popstate", function () {
                let name = window.location.pathname.substr(1);
                app.selectWord(name);
            });
            let app = new Vue({
                el: "#app",
                data: {
                    alphabet: [],
                    letter: '',
                    words: [],
                    loadingComments: true,
                    word: {},
                    menuVisible: false,
                    exception: {
                        show: false,
                        message: "",
                        duration: 2000
                    },
                    popular: [],
                    newest: [],
                    comment: "",
                    query: "",
                    user: {
                        username: "",
                        password: "",
                        permissions: []
                    },
                    dialogFix: false,
                    wordDialogData: {
                        "title": "Create",
                        "id": '',
                        "name": '',
                        "content": "",
                        "tags": [],
                        "filters": [],
                        "close": false
                    },
                    filterDialog: {
                        title: "Edit Filter",
                        item: {},
                        open: false
                    },
                    filters: [],
                    wordTags: [],
                    showLoginDialog: false,
                    showUserDialog: false,
                    waitForOpenLoginDialog: true,
                    showSearchPanel: false,
                    showWordDialog: false,
                    showUserEditDialog: false,
                    showFiltersDialog: false
                },
                computed: {
                    isOwner: function () {
                        let user_id = false;
                        @if(!Auth::guest())
                            user_id = {{Auth::user()->id}};
                        @endif
                        if (!user_id) {
                            return false;
                        }
                        return this.word.author.id == user_id;
                    },
                    list: function () {
                        return this.words.sort(function (a, b) {
                            let str1 = a.name.toUpperCase();
                            let str2 = b.name.toUpperCase();
                            return (str1 < str2) ? -1 : (str1 > str2) ? 1 : 0;
                        });
                    },
                    popularList: function () {
                        return this.popular.map(function (item) {
                            return item.name;
                        })
                    },
                    filterCount: function () {
                        if (this.word && this.word.filters) {
                            let leng = 0;
                            for (let s in this.word.filters) {
                                if (this.word.filters[s].id) {
                                    leng++;
                                }
                            }
                            return leng;
                        }
                        return 0;
                    }
                },
                created: function () {
                    this.waitForOpenLoginDialog = @if(Auth::guest()) true;
                    @else false;
                    @endif;
                    this.getUserPermissions();
                    this.autoOpenLoginDialog();
                    this.loadAlphabet();
                    this.getPopular();
                    this.getNewest();
                    this.getFilterList();
                    @if($word)
                        this.selectWord("{{$word}}");
                    @else
                        this.loadList();
                    @endif

                },
                methods: {
                    goUserAdmin: function(){
                        window.location = "{{route("dashboard:users")}}";
                    },
                    openFilters: function () {
                        this.getFilters();
                        this.showFiltersDialog = true;
                    },
                    openFilter: function (item) {
                        let self = this;
                        self.filterDialog.open = true;
                        self.filterDialog.item = item;
                        self.showFiltersDialog = false;
                    },
                    openFilterToCreate: function () {
                        let self = this;
                        self.filterDialog.open = true;
                        self.filterDialog.item = {
                            "name" : ""
                        };
                        self.showFiltersDialog = false;
                    },
                    noSaveFilter: function () {
                        let self = this;
                        self.getFilters();
                        self.filterDialog.open = false;
                        self.showFiltersDialog = true;
                    },
                    createFilter: function () {
                        let self = this;
                        axios.post("{{route("side:filter:create")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "name": self.filterDialog.item.name
                            }).then(function (response) {
                            self.noSaveFilter();
                        });
                    },
                    editFilter: function () {
                        let self = this;
                        axios.put("{{route("side:filter:put")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "id": self.filterDialog.item.id,
                                "name": self.filterDialog.item.name
                            }).then(function (response) {
                            self.noSaveFilter();
                        });
                    },

                    /** UP UP UP **/
                    checkLoginState: function () {
                        let self = this;
                        FB.getLoginStatus(function (response) {
                            if (response && response.status !== 'connected') {
                                FB.login(function (response) {
                                    self.statusChangeCallback(response);
                                }, {scope: 'public_profile,email'});
                            } else {
                                self.statusChangeCallback(response);
                            }
                        });
                    },
                    statusChangeCallback: function (response) {
                        let data = response.authResponse;
                        if (data && data.accessToken) {
                            axios.post("{{route("api:fb:login")}}", {
                                "_token": CSRF_TOKEN,
                                "token": data.accessToken
                            }).then(function (response) {
                                let data = response.data;
                                if (data.redirect) {
                                    window.location = data.redirect;
                                }
                            })
                        } else {
                            let message = "Canceled";
                            this.showException(message);
                        }
                    },
                    toggle: function (item) {
                        let self = this;
                        axios.post("{{route('admin:active')}}", {
                            "_token": CSRF_TOKEN,
                            "id": item.id,
                        }).then(function (response) {
                            let status = response.data.status;
//                            self.word.status = status;
                            item.status = status;
                        });
                    },
                    md5: function (text) {
                        return md5(text);
                    },
                    getUserPermissions: function () {
                        let self = this;
                        axios.get("{{route('user:permissions')}}")
                            .then(function (response) {
                                let permissions = response.data.permissions;
                                self.user.permissions = permissions;
                            });
                    },
                    openWordDialog: function () {
                        this.wordDialogData.id = null;
                        this.wordDialogData.name = null;
                        this.wordDialogData.content = null;
                        this.wordDialogData.tags = [];
                        this.wordDialogData.filters = [];
                        this.showWordDialog = true;
                        this.getFilterList();
                    },
                    editWordDialog: function () {
                        this.wordDialogData.id = this.word.id;
                        this.wordDialogData.name = this.word.name;
                        this.wordDialogData.content = this.word.description;
                        this.wordDialogData.tags = this.word.tags;
                        this.wordDialogData.filters = this.word.filters_id;
                        this.showWordDialog = true;
                        this.getFilterList();
                    },
                    getFilterList: function () {
                        let self = this;
                        axios
                            .get("{{route('side:filter:get')}}")
                            .then(function (response) {
                                let list = response.data.list;
                                self.filters = list;
                            });
                    },
                    getFilters: function () {
                        let self = this;
                        axios
                            .get("{{route('side:filters:get')}}")
                            .then(function (response) {
                                self.wordTags = response.data.list;
                            });
                    },
                    saveWord: function () {
                        let self = this;
                        axios.post("{{route("admin:create")}}", {
                            "_token": CSRF_TOKEN,
                            "id": self.wordDialogData.id,
                            "name": self.wordDialogData.name,
                            "description": self.wordDialogData.content,
                            "tags": self.wordDialogData.tags.map(function (tag) {
                                return tag.name;
                            }),
                            "filters": self.wordDialogData.filters,
                        }).then(function (response) {
                            self.showWordDialog = false;
                            self.selectWord(response.data.word.name);
                        });
                    },
                    getUserInformation: function (id) {
                        let self = this;
                        axios.post("{{route("side:user")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "id": id,
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let permissions = self.user.permissions;
                                self.user = response.data.user;
                                self.user.permissions = permissions;
                                self.showUserDialog = true;
                            }
                        });
                    },
                    getUserInformationToEdit: function (id) {
                        let self = this;
                        axios.post("{{route("side:user")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "id": id,
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let permissions = self.user.permissions;
                                self.user = response.data.user;
                                self.user.permissions = permissions;
                                self.showUserEditDialog = true;
                            }
                        });
                    },
                    saveUserInformation: function () {
                        let self = this;
                        axios.put("{{route("side:user:update")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "id": self.user.id,
                                "name": self.user.name,
                                "age": self.user.details.age,
                                "location": self.user.details.locale,
                                "gender": self.user.details.gender,
//                                "password" : self.password
                            }
                        ).then(function (response) {
                            if (response.data) {
                                self.showUserEditDialog = false;
                            }
                        });
                    },
                    autoOpenLoginDialog: function () {
                        let self = this;
                        setTimeout(function () {
                            if (self.waitForOpenLoginDialog) {
                                self.showLoginDialog = true;
                                self.waitForOpenLoginDialog = false;
                            }
                        }, 10000)
                    },
                    openLoginDialog: function () {
                        this.showLoginDialog = true;
                        this.waitForOpenLoginDialog = false;
                    },
                    closeLoginDialog: function () {
                        this.showLoginDialog = false;
                        this.waitForOpenLoginDialog = false;
                    },
                    logout: function () {
                        let self = this;
                        axios.post("{{route("auth:logout")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "word": self.word.name
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let redirect = response.data.redirect;
                                setTimeout(function () {
                                    document.location.href = redirect
                                }, 1000);
                            }
                        });
                    },
                    showException(exception) {
                        this.exception.show = true;
                        this.exception.message = exception;
                    },
                    find: function () {
                        let self = this;
                        self.letter = '';
                        if (!self.query) {
                            self.showException("Please insert search word");
                            return false;
                        }
                        axios.post("{{route("word:find")}}",
                            {
                                "word": self.query,
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let word = response.data.word;
                                self.selectWord(word.name);
                            }
                        }).catch(function (error) {
                            if (error.response && error.response.data) {
                                let message = error.response.data.error[0];
                                self.showException(message);
                            }
                        });
                    },
                    selectLetter(letter) {
                        this.letter = letter;
                        this.loadList();
                        this.getPopular();
                        this.getNewest();
                    },
                    toggleMenu() {
                        this.menuVisible = !this.menuVisible
                    },
                    processTime: function (time) {
                        return moment(time).fromNow();
                    },
                    loadAlphabet: function () {
                        let self = this;
                        axios.post('{{route("word:alphabet")}}')
                            .then(function (response) {
                                if (response.data) {
                                    if (response.data.alphabet) {
                                        self.alphabet = response.data.alphabet;
                                    }
                                }
                            })
                            .catch(function (error) {
                                if (error.response && error.response.data) {
                                    let message = error.response.data.message;
                                    self.showException(message);
                                }
                            });

                    },
                    selectWord: function (word) {
                        let self = this;
                        self.word = {};
                        axios.post("{{route("word:get")}}",
                            {
                                "word": word,
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let word = response.data.word;
                                let collection = response.data.collection;
                                self.word = word;
                                history.pushState({}, "", word.name);
                                self.words = collection;
                                self.loadComments();
                                self.setMetaTags();
                            }
                        }).catch(function (error) {
                            if (error.response && error.response.data) {
                                let message = error.response.data.errors[0];
                                self.showException(message);
                            }
                        });
                    },
                    loadList: function () {
                        let self = this;
                        self.word = {};
                        axios.post("{{route("word:random")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let word = response.data.word;
                                let collection = response.data.collection;
                                self.word = word;
                                self.words = collection;
                                self.loadComments();
                            }
                        }).catch(function (error) {
                            console.log(error);
                        });
                    },
                    getPopular: function () {
                        let self = this;
                        axios.post("{{route("word:popular")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        ).then(function (response) {
                            if (response.data && response.data.collection) {
                                self.popular = response.data.collection;
                            }
                        }).catch(function (error) {
//                            self.getPopular();
                        });
                    },
                    getNewest: function () {
                        let self = this;
                        axios.post("{{route("word:newest")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        ).then(function (response) {
                            if (response.data && response.data.collection) {
                                self.newest = response.data.collection;
                            }
                        }).catch(function (error) {
//                            self.getPopular();
                        });
                    },
                    loadComments: function () {
                        let self = this;
                        self.loadingComments = true;
                        axios.post("{{route("word:comments")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "id": self.word.id,
                                "name": self.word.name
                            }
                        ).then(function (response) {
                            if (response.data && response.data.comments) {
                                self.word.comments = response.data.comments;
                                self.loadingComments = false;
                            }
                        }).catch(function (error) {
                            if (error.response && error.response.data) {
                                self.showException("Server Error");
                            }
                        });
                    },
                    commentAction: function () {
                        let self = this,
                            comment = self.comment;
                        self.comment = null;
                        axios.put("{{route("word:comment")}}",
                            {
                                "_token": CSRF_TOKEN,
                                "content": comment,
                                "word_id": self.word.id
                            }
                        ).then(function (response) {
                            if (response.data && response.data.comments) {
                                self.word.comments = response.data.comments;
                                self.comment = '';
                            }
                        }).catch(function (error) {
                            self.showException("Sorry, but comment not send");
                            self.comment = comment;
                        });
                    },
                    login: function () {
                        let self = this;
                        axios.post("{{route('auth:login')}}",
                            {
                                "_token": CSRF_TOKEN,
                                "email": self.user.username,
                                "password": self.user.password,
                                "word": self.word.name
                            }
                        ).then(function (response) {
                            if (response.data) {
                                let redirect = response.data.redirect;
                                if (redirect) {
                                    self.closeLoginDialog();
                                    setTimeout(function () {
                                        document.location.href = redirect
                                    }, 1000);
                                }
                            }
                        }).catch(function (error) {
                            self.showException("Invalid credentials");
                        });
                    },
                    like: function (word) {
                        let self = this,
                            url = "{{route("word:like")}}";
                        if (word.like) {
                            url = "{{route("word:dislike")}}"
                        }

                        axios.post(url,
                            {
                                "_token": CSRF_TOKEN,
                                "word_id": word.id,
                                "word": word.name
                            }
                        ).then(function (response) {
                            if (response.data) {
                                self.word.like = response.data.like;
                                self.word.likes = response.data.likes;
                            }
                        }).catch(function (error) {
                            if (error.response && error.response.data) {
                                let message = error.response.data.message;
                                self.showException(message);
                            }
                        });
                    },
                    setMetaTags: function () {
                        $('title').text("Slangshot::" + this.word.name);
                        $('meta[name=description]').attr('content', this.word.description);
                        $('meta[name=author]').attr('content', this.word.author.name);
                        $("#og_url").attr('content', window.location);
                        $("#og_title").attr('content', this.word.name);
                        $("#og_description").attr('content', this.word.description);
                        /** TODO BLYA!!! **/
                    },

                }
            });
        });
    </script>
@endsection