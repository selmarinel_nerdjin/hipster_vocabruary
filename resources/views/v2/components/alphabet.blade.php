<div class="alphabet">
    <i class="letter" @click="selectLetter('')">@lang('home.all')</i>
    <md-menu md-direction="bottom-end" v-for="uno in alphabet">
        <i md-menu-trigger class="letter" :class="{'active':letter == uno}">@{{ uno }}</i>
        <md-menu-content>
            <md-menu-item @click="selectLetter(uno)">
                Filter : @{{ uno }}
            </md-menu-item>
            <md-menu-item>
                List by : @{{ uno }}
            </md-menu-item>
        </md-menu-content>
    </md-menu>
    <md-button class="md-icon-button" @click="openWordDialog" v-if="user.permissions[md5('can_add_word')]">
        <md-icon>add</md-icon>
        <md-tooltip>
            Add new word
        </md-tooltip>
    </md-button>
</div>