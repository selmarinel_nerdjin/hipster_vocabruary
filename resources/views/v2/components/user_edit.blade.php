<md-dialog :md-active.sync="showUserEditDialog">
    <md-dialog-title>
        Edit User information
    </md-dialog-title>
    <md-dialog-content class="container">
        <div class="row">
            <div class="col-4 text-right">
                <md-avatar class="md-large">
                    <img :src="user.cover" alt="People">
                </md-avatar>
            </div>
            <div class="col-8">
                {{--<h5>@{{ user.name }}</h5>--}}
                <small>@{{ user.email }}</small>
            </div>

        </div>
        <div class="row" v-if="user.details">
            <form class="col" novalidate>
                <md-field>
                    <label for="name">Name</label>
                    <md-input name="name" id="name" v-model="user.name" required/>
                </md-field>
                <md-field>
                    <label for="age">@lang("home.age")</label>
                    <md-input type="number" name="age" id="age" v-model="user.details.age" required/>
                </md-field>
                <md-radio v-model="user.details.gender" value="alien">Alien</md-radio>
                <md-radio v-model="user.details.gender" value="male">Male</md-radio>
                <md-radio v-model="user.details.gender" value="female">Female</md-radio>
                <md-field>
                    <label for="age">@lang("home.address")</label>
                    <md-input name="address" id="address" v-model="user.details.locale" required/>
                </md-field>
            </form>
        </div>
    </md-dialog-content>
    <md-dialog-actions>
        <md-button class="md-raised md-delete" @click="showUserEditDialog = false">Close</md-button>
        <md-button class="md-raised md-activate" @click="saveUserInformation">Save</md-button>
    </md-dialog-actions>
</md-dialog>