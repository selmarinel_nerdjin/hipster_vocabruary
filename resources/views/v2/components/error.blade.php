<md-snackbar md-position="center"
             :md-active.sync="exception.show"
             :md-duration="exception.duration"
             md-persistent>
    <span>@{{ exception.message }}</span>
    <md-button class="md-raised md-primary btn btn-dark"
               @click="exception.show = false">
        OK
    </md-button>
</md-snackbar>