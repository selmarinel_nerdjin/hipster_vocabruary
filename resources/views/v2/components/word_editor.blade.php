<md-dialog :md-active.sync="showWordDialog"
           class="md-scrollbar"
           :md-click-outside-to-close=false
           style="width: 90%">
    {{--<md-dialog-title>@{{ wordDialogData.title }} Word</md-dialog-title>--}}

    <md-tabs class="md-primary" md-alignment="centered">
        <md-tab id="tab-home" md-icon="chrome_reader_mode" :md-active-tab.sync="showWordDialog">
            <form novalidate>
                <md-field>
                    <label>Word Name</label>
                    <md-input v-model="wordDialogData.name"></md-input>
                </md-field>
                <word_tags
                        :tags="wordDialogData.tags">
                </word_tags>
                <vue-editor
                        placeholder="Word Description"
                        v-model="wordDialogData.content">
                </vue-editor>
            </form>
        </md-tab>
        <md-tab id="tab-pages" md-icon="settings_input_component">
            <md-switch v-model="wordDialogData.filters[key]" v-for="filter,key in filters">
                @{{ filter }}
            </md-switch>
        </md-tab>

    </md-tabs>

    <md-dialog-actions>
        <md-button class="md-raised md-delete" @click="showWordDialog = false">Cancel</md-button>
        <md-button class="md-raised md-activate" @click="saveWord()">Save</md-button>
    </md-dialog-actions>
</md-dialog>
