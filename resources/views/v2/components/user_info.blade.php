<md-dialog :md-active.sync="showUserDialog">
    <md-dialog-title>
        User information
    </md-dialog-title>
    <md-dialog-content class="container">
        <div class="row">
            <div class="col">
                <h5>
                    @{{ user.name }} <br/>
                    <small>@{{ user.email }}</small>
                </h5>
            </div>
            <div class="col text-right">
                <md-avatar class="md-large">
                    <img :src="user.cover" alt="People">
                </md-avatar>
            </div>
        </div>
        <div class="row" v-if="user.details">
            <div class="col">
                <div>
                    <b>@lang("home.age") : @{{ user.details.age }}</b>
                </div>
                <div>
                    <b>@lang("home.gender") : @{{ user.details.gender }}</b>
                </div>
                <div>
                    <b>@lang("home.address") : @{{ user.details.locale }}</b>
                </div>
            </div>
        </div>
    </md-dialog-content>
    <md-dialog-actions>
        <md-button class="md-primary md-raised" @click="showUserDialog = false">Close</md-button>
    </md-dialog-actions>
</md-dialog>