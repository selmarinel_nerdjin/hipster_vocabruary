<div class="row" v-if="word.tags && word.tags.length">
    <md-card class="col-12 ltcard">
        <md-card-header>
            <h5 class="exo word-title">Synonims</h5>
        </md-card-header>
        <md-card-content>
            <md-list>
                <md-list-item class="word"
                              v-for="item in word.tags">
                                            <span class="md-list-item-text" @click="selectWord(item.name)">
                                                <div class="name">@{{ item.name }}</div>
                                                <md-tooltip>
                                                    <md-icon>favorite</md-icon>
                                                    @{{ item.likes }}
                                                    <md-icon>comment</md-icon>
                                                    @{{ item.comments }}
                                                    <md-icon>class</md-icon>
                                                    @{{ item.tags }}
                                                </md-tooltip>
                                            </span>
                    <div>
                        <md-button class="md-icon-button md-mini md-primary md-raised"
                                   @click="selectWord(item.name)">
                            <md-icon>
                                info
                            </md-icon>
                        </md-button>
                        <md-button class="md-icon-button md-mini"
                                   @click="toggle(item)"
                                   v-if="user.permissions[md5('can_verify_word')]"
                                   :class="(item.status != 1)? 'md-activate':'md-delete'">
                            <md-icon>
                                @{{ (item.status != 1)?'visibility':'visibility_off' }}
                            </md-icon>
                        </md-button>
                    </div>

                </md-list-item>
            </md-list>
        </md-card-content>
    </md-card>
</div>