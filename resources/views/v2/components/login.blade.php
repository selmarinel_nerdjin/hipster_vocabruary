<md-dialog :md-active.sync="showLoginDialog">
    <md-dialog-title class="underline">
        Login
        <small>to Slangshot</small>
    </md-dialog-title>
    <md-dialog-content>
        <form novalidate @submit.prevent="login">
            <div class="md-layout-row md-layout-wrap md-gutter">
                <div class="md-flex md-flex-small-100">
                    <md-field md-clearable>
                        <md-icon class="md-primary">perm_identity</md-icon>
                        <label>Login / email</label>
                        <md-input v-model="user.username"></md-input>
                    </md-field>

                    <md-field>
                        <md-icon class="md-primary">lock</md-icon>
                        <label>Password</label>
                        <md-input type="password" v-model="user.password"></md-input>
                    </md-field>

                    <div class="text-center">
                        <md-button
                                type="submit"
                                class="md-primary md-raised btn btn-dark">
                            @lang("home.enter")
                        </md-button>
                    </div>
                    <div class="text-center roboto">
                        <small class="">With your social media account</small>
                    </div>
                    <div class="text-center">
                        <button type="button"
                                class="btn btn-primary btn-sm"
                                @click="checkLoginState">
                                    <span class="social-icon">
                                        <i class="fa fa-facebook"></i>
                                    </span>
                            <span class="social-text">
                                        Facebook
                                    </span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </md-dialog-content>
</md-dialog>