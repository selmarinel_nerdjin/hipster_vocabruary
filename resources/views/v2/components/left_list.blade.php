<md-app-drawer class="white md-scrollbar" :md-active.sync="menuVisible" md-persistent="full">
    <md-toolbar md-elevation="0">
        <span>@lang("home.word_list")</span>
        <div class="md-toolbar-section-end">
            <md-button class="md-icon-button md-dense" @click="toggleMenu">
                <md-icon>keyboard_arrow_left</md-icon>
            </md-button>
        </div>
    </md-toolbar>
    <md-list>
        <md-list-item
                class="word"
                v-for="item in list"
                @click="selectWord(item.name)"
                :class="{active:item.name == word.name}">
            <div class="md-list-item-text">
                <span class="name">@{{ item.name }}</span>
                <div class="date">
                    @{{ processTime(item.created_at) }}
                </div>
            </div>
        </md-list-item>

        <md-divider class="md-inset"></md-divider>
    </md-list>
</md-app-drawer>