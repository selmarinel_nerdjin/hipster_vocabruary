@extends('layouts.app')

@section('title')
    @if($word)
        SlangShot::{{$word}}
    @else
        SlangShot::Word
    @endif
@endsection

@section('meta')
    <meta property="og:url" content="{{route("home",["word" => $word])}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$wordModel->name}}"/>
    <meta property="og:description" content="{{$wordModel->description}}"/>
    <meta name="description" content="{{$wordModel->description}}"/>
    <meta name="keywords" content=""/>
    <meta name="author" content="{{$wordModel->getAuthor()->name}}"/>
@endsection

@section('css')
    <style>
        body{
            background-color: #fcfcfc !important;
        }
        .section_user {
            padding: 25px 0;
            background-color: #607d8b;
        }

        .auth {
            max-height: 400px;
            min-width: 400px;
            align-self: center;
            padding: 10px;
        }

        .xs-large {
            width: 150px !important;
            height: 150px !important;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
        }

        .md-sidenav-content {
            position: fixed !important;
        }

        .md-right .md-sidenav-content {
            min-width: 40vw !important;
        }

        .admin_dashboard {
            position: relative;
            overflow: hidden;
            height: 100vh;
        }

        .md-success {
            background-color: #2ecc71 !important;
        }

        .md-success i {
            color: white !important;
        }

        .md-ban {
            background-color: #e74c3c !important;
        }

        .md-ban i {
            color: white !important;
        }

        .md-editor {
            background-color: #2c3e50 !important;
        }

        .md-editor i {
            color: white !important
        }

        .md-moderator {
            background-color: #8e44ad !important;
        }

        .md-moderator i {
            color: white !important
        }
    </style>
    <style>
        .md-badge {
            min-height: 0 !important;
            min-width: 0 !important;
            padding: 5px !important;
            margin: 5px 0 !important;
            font-size: 11px !important;
        }

        .user-avatar {
            background-color: rgba(255, 255, 255, 0.9);
            border-radius: 50% !important;
            margin: -10px;
            padding: 2px;
            border: outset 3px #a5a5a5;
        }

        .trumbowyg-box, .trumbowyg-editor {
            min-height: 100px !important;
        }

        #wordEdit {
            display: none;
        }
    </style>
    <style>
        .word-edit .md-dialog {
            width: 95% !important;
            height: 95vh !important;
            max-height: inherit !important;
        }

        .word-edit-content::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 5px;
            background-color: #F5F5F5;
        }

        .word-edit-content::-webkit-scrollbar {
            width: 7px;
            background-color: #F5F5F5;
        }

        .word-edit-content::-webkit-scrollbar-thumb {
            border-radius: 5px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: #555;
        }

        .title-section {
            display: block;
            width: 100%;
            height: 45px;
            text-align: center;
            padding: 10px;
            margin-left: -5px;
            margin-right: -5px;
        }

        .title-section.general-section {
            color: white;
            background-color: #34495e;
        }

        .title-section.filter-section {
            color: white;
            background-color: #8e44ad;
        }

        .word-edit .md-dialog-title {
            padding: 5px 12px 0px !important;
            margin-bottom: 5px;
        !important;
        }

        .word-edit .md-dialog-content {
            padding: 5px !important;
        }
    </style>
@endsection

@section('content')
    @include('layouts.components.sidenav.left')
    @include('layouts.components.sidenav.right')
    @include('layouts.components.sidenav.word_filter')
    @include('layouts.components.dialog.user')
    @include('layouts.components.dialog.user_login')
    @include('layouts.components.dialog.word_edit')
    @include('layouts.components.dialog.exception_notfound')
    <sn-registration ref="register"
                     :title='"Edit Information"'
                     :update_url='"{{route("side:user:update")}}"'
                     :token="crf_token">
    </sn-registration>
    <section id="content">
        <md-toolbar>
            <md-button class="md-icon-button" @click="toggleLeftSidenav">
                <md-icon>menu</md-icon>
            </md-button>
            <md-avatar>
                <img src="{{asset("img/em.png")}}" alt="Avatar">
            </md-avatar>
            <h2 class="md-title" style="flex: 1">
                &nbsp;Slangshot<span v-if="letter" class="badge">@{{letter}}</span>
            </h2>
            <form novalidate @submit.stop.prevent="submit" class="form-inline">
                <md-input-container md-inline>
                    <md-input v-model="finder" style="width:150px"
                              placeholder="@lang('home.search') @lang('home.word')"></md-input>
                    <md-button class="md-icon-button" @click="findWord()">
                        <md-icon>search</md-icon>
                    </md-button>
                </md-input-container>

            </form>
        </md-toolbar>
        <div class="container-fluid">
            <div class="col-xs-12 text-center">
                <ul class="list-inline" v-if="alphabet.length">
                    <li class="list-item-circle" v-bind:class="{active: letter == ''}"
                        @click="selectLetter('')">@lang('home.all')
                    </li>
                    <li class="list-item-circle"
                        v-for="l in alphabet"
                        v-bind:class="{active: letter == l}"
                        @click="selectLetter(l)">@{{ l }}</li>
                </ul>
                <div v-if="!alphabet.length">
                    <md-progress md-indeterminate></md-progress>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-5 hidden-xs">
                <md-whiteframe md-tag="section">
                    <md-subheader>
                        <b class="big">@lang('home.word_list')</b>
                        <md-button class="md-icon-button holder" @click="updateList()">
                            <md-icon>cached</md-icon>
                        </md-button>

                        <md-button class="md-icon-button" v-if="admin || rules.can_add_word" @click="createWord()">
                            <md-icon>library_add</md-icon>
                        </md-button>
                        <md-button class="md-icon-button" @click="changeMode">
                            <md-icon>@{{(modeView)?'view_headline':'view_agenda'}}</md-icon>
                        </md-button>
                    </md-subheader>
                    <md-list class="custom-list" v-bind:class="{'md-triple-line md-dense':modeView}">
                        <md-list-item v-for="item,key in sortList"
                                      @click="selectWord(item.name)"
                                      v-bind:class="{active:checkWord(item.name)}">
                            <div class="md-list-text-container">
                                <span>@{{ item.name }}</span>
                                <span v-if="modeView" v-html="convertDate(item.created_at)"></span>
                                <p v-if="modeView">
                                    <md-icon class="md-like">favorite</md-icon>@{{ item.likes }}
                                    <md-icon class="md-comment">comment</md-icon>@{{ item.comments }}
                                    <md-icon class="md-links">link</md-icon>@{{ item.tags }}
                                </p>
                            </div>
                            <md-divider class="md-inset" v-if="modeView"></md-divider>
                            <md-button
                                    class="md-icon-button md-list-action"
                                    v-if="admin || rules.can_verify_word"
                                    @click="hide(item)"
                                    v-bind:class="{disable:!item.status,submitted:(item.status === {{\App\Models\Word::SUBMITTED}})}">
                                <md-icon class="md-primary">
                                    @{{ (item.status === 1)?"visibility_off":"visibility" }}
                                </md-icon>
                            </md-button>
                        </md-list-item>
                    </md-list>
                    <md-bottom-bar md-shift>
                        <md-bottom-bar-item md-icon="info" md-active>
                            @{{ wordsCount }}
                        </md-bottom-bar-item>
                    </md-bottom-bar>
                </md-whiteframe>
            </div>
            {{--WORD CONTENT--}}
            <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
                <section v-if="word">
                    <md-card>
                        <md-card-header>
                            <h1>
                                <span>
                                <md-icon class="text-danger" v-show="word.status != 1">
                                    block
                                </md-icon>
                                <md-tooltip md-direction="bottom">
                                    @lang('home.word_is_not_verified')
                                </md-tooltip>
                                </span>
                                @{{ word.name }}
                                <md-button class="md-icon-button md-raised"
                                           @click="editWord"
                                           v-if="admin ||
                                            rules.can_edit_word
                                            @if(!Auth::guest() && Auth::user())
                                                   || word.author.email == '{{Auth::user()->email}}'
                                            @endif
                                                   ">
                                    <md-icon>create</md-icon>
                                </md-button>
                            </h1>
                        </md-card-header>
                        <md-card-content>
                            <p v-html="word.description"></p>
                            <div style="padding:5px">
                                <div class="badge" @click="selectRelate(tag.name)" v-for="tag in word.tags">
                                    @{{ tag.name }}
                                    <md-tooltip md-direction="bottom">
                                        <md-icon>favorite</md-icon> @{{ tag.likes }}
                                        <md-icon>comments</md-icon> @{{ tag.comments }}
                                        <md-icon>link</md-icon>@{{ tag.tags }}
                                    </md-tooltip>
                                </div>
                            </div>
                            <div v-if="word.filters">
                                <span class="label label-default" v-for="filter in word.filters">
                                    @{{ filter.name }}
                                </span>
                            </div>
                        </md-card-content>
                        <md-card-actions>
                            <div class="md-subhead" style="flex: 1;" v-if="word">@lang('home.by') <span
                                        class="user_button"
                                        @click="getUserInformation(word.author.id)">@{{ word.author.name }}</span>
                                @lang('home.at') @{{ createdFormat }}</div>
                            <span class="badge">@{{ likesList }}</span>
                            <md-button class="md-icon-button" v-bind:class="{liked:word.like}" @click="like(word)">
                                <md-icon>favorite</md-icon>
                            </md-button>

                            <social-sharing :url="'{{route("home")}}/'+word.name" inline-template>
                                <network network="facebook">
                                    <md-button>
                                        Share on Facebook
                                        <md-icon>share</md-icon>
                                    </md-button>
                                </network>
                            </social-sharing>
                        </md-card-actions>
                    </md-card>
                    <br>
                    <md-card>
                        <md-subheader>@lang('home.comments')</md-subheader>
                        <md-card-content>
                            @if(!Auth::guest())
                                <form novalidate @submit.stop.prevent="commentAction" id="commentForm">
                                    <md-input-container>
                                        <label>@lang('home.comment')</label>
                                        <md-textarea
                                                maxlength="255"
                                                v-model="comment"
                                        ></md-textarea>
                                    </md-input-container>
                                    <md-button class="md-raised md-mini" type="commentAction">
                                        <span>@lang('home.send')</span>
                                        <md-icon>send</md-icon>
                                    </md-button>
                                </form>
                            @else
                                <div class="text-center">
                                    @lang('home.notice_register')
                                    <div>
                                        <md-button class="md-dense"
                                                   @click="openLoginDialog()">@lang('home.login')</md-button>
                                    </div>
                                </div>
                            @endif
                        </md-card-content>
                        <md-card-content>
                            <md-list class="custom-list md-triple-line">
                                <section v-for="item,key in word.comments">
                                    <md-list-item>
                                        <md-avatar>
                                            <img v-bind:src="item.user.cover" alt="People">
                                        </md-avatar>

                                        <div class="md-list-text-container">
                                        <span class="user_button"
                                              @click="getUserInformation(item.user.id)">@{{ item.user.name }}</span>
                                            <span>@{{ fromDate(item.created_at) }}</span>
                                        </div>

                                        <md-button class="md-icon-button md-raised md-list-action" v-if="admin"
                                                   @click="deleteComment(item.id)">
                                            <md-icon class="md-primary">delete</md-icon>
                                        </md-button>
                                    </md-list-item>
                                    <md-list-item class="md-inset">
                                        <div class="md-list-text-container">
                                            <p v-html="item.content"></p>
                                        </div>

                                        <md-divider class="md-inset"></md-divider>
                                    </md-list-item>
                                </section>
                            </md-list>
                        </md-card-content>
                    </md-card>
                </section>
                <section v-if="!word">
                    <md-card>
                        <md-card-header>
                            @lang('home.search') @lang('home.word')
                        </md-card-header>
                        <div class="text-center">
                            <img src="assets/giphy.gif" alt="People" width="300px">
                        </div>
                        <md-progress class="md-accent" md-indeterminate></md-progress>
                    </md-card>
                </section>
            </div>
        </div>
    </section>
    <md-snackbar :md-position="'bottom right'" ref="snackbar" :md-duration="4000">
        <span>@{{ exceptionObject.content }}</span>
    </md-snackbar>
@endsection

@section('scripts')
    @inject('permissions', 'App\Services\PermissionService')
    <script>
        $(document).ready(function () {
            history.pushState('', document.title, window.location.pathname);

            channel.bind('words_count', function (data) {
                app.wordsCount = data;
            });
            window.addEventListener("popstate", function () {
                var name = window.location.pathname.substr(1);
                app.selectWord(name);
            });

            $("body").on("keyup", "#commentForm textarea", function (e) {
                /** todo: **/
                if (e.keyCode == 13 && e.ctrlKey) {
                    app.commentAction();
                }
            });

            const FailParemeters = "Incorrect search parameters";
            const FailSearch = "Your word not found. Please input another word and try again.";

            var app = new Vue({
                el: '#app',
                data: {
                    //users administrate
                    users: [],
                    //main
                    alphabet: [],
                    list: [],
                    modeView: false,
                    word: "{{$word}}",
                    finder: '',
                    letter: "",
                    selected: null,
                    wordsCount: "{{$count}}",
                    alertObject: {
                        title: "Word not found",
                        text: "Your word not found. Please input another word and try again.",
                    },
                    viewUser: null,
                    dialog: {
                        title: "Edit Word",
                        name: "",
                        content: "",
                        id: "",
                        tags: [],
                        filters: []
                    },
                    registerFlag: false,
                    options: [],
                    filters: [],
                    exceptionObject: {
                        content: FailParemeters
                    },
                    comment: "",
                    usersPage: 1,
                    usersPages: 1,
                    pagesLimit: 5,
                    userListFinder: "",
                    user: {
                        filters: {},
                        email: '',
                        password: '',
                        log: @if(!Auth::guest() && Auth::user()) true @else false @endif
                    },
                    rules: {
                        @if(!Auth::guest() && Auth::user() && $permissions->can(Auth::user(),\App\Models\Rule::CAN_ADD_WORD))
                        can_add_word: true,
                        @else
                        can_add_word: false,
                        @endif
                                @if(!Auth::guest() && Auth::user() && $permissions->can(Auth::user(),\App\Models\Rule::CAN_EDIT_WORD))
                        can_edit_word: true,
                        @else
                        can_edit_word: false,
                        @endif
                                @if(!Auth::guest() && Auth::user() && $permissions->can(Auth::user(),\App\Models\Rule::CAN_VERIFY))
                        can_verify_word: true,
                        @else
                        can_verify_word: false,
                        @endif
                    },
                    admin: @if(!Auth::guest() && Auth::user() && Auth::user()->isAdmin()) true @else false @endif

                },
                computed: {
                    crf_token: function () {
                        return CSRF_TOKEN;
                    },
                    sortList: function () {
                        return this.list.sort(function (a, b) {
                            var str1 = a.name.toUpperCase();
                            var str2 = b.name.toUpperCase();
                            return (str1 < str2) ? -1 : (str1 > str2) ? 1 : 0;
                        });
                    },
                    createdFormat: function () {
                        if (this.word.created_at) {
                            return moment(this.word.created_at).format("dddd, MMMM Do YYYY");
                        }
                    },
                    likesList: function () {
                        if (!this.word) {
                            return 0;
                        }
                        if (this.word.people.length > 1) {
                            return this.word.people[0].name + " and " + (this.word.people.length - 1) + " more";
                        } else if (this.word.people.length > 5) {
                            return this.word.likes
                        } else if (this.word.people[0]) {
                            return this.word.people[0].name
                        } else {
                            return '';
                        }
                    },
                },
                created: function () {
                    let self = this;
                    if (this.word) {
                        this.selectWord(this.word)
                    } else {
                        this.loadList();
                    }
                    this.getAlphabet();
                    this.getFilterList();
                    @if(Auth::guest())
                        setTimeout(function () {
                        if (!self.registerFlag) {
                            self.$refs['login'].open();
                        }
                    }, 10000);
                    @endif
                    console.log(self.rules.can_add_word);
                },
                updated: function () {
                    if (this.word) {
                        $('meta[property="og:url"]').attr('content', "{{route("home")}}/" + this.word.name);
                        $('meta[property="og:type"]').attr('content', "website");
                        $('meta[property="og:title"]').attr('content', this.word.name);
                        $('meta[property="og:description"]').attr('content', this.word.description);
                        $('meta[name="description"]').attr('content', this.word.name);
                        $('meta[name="keywords"]').attr('content', this.word.name);
                        $('meta[name="author"]').attr('content', this.word.author.name);
                        $("title").text("SlangShot::" + this.word.name);
                    }
                },
                methods: {
                    //
                    editFilter: function (id, filter) {
                        let self = this;
                        $.confirm({
                            "title": "Edit filter ?",
                            "content": "Are u sure?",
                            buttons: {
                                cancel: function () {
                                },
                                edit: {
                                    text: "Edit",
                                    action: function () {
                                        $.ajax({
                                            url: "{{route("side:filter:put")}}",
                                            dateType: "json",
                                            type: "put",
                                            data: {
                                                "_token": CSRF_TOKEN,
                                                "id": id,
                                                "name": filter
                                            }
                                        }).done(function (response) {
                                            self.getFilterList();
                                        }).fail(function () {
                                            self.openSnack("Failed");
                                        })
                                    }
                                }
                            }
                        });

                    },
                    deleteFilter: function (id) {
                        let self = this;
                        $.confirm({
                            "title": "Delete filter ?",
                            "content": "Are u sure?",
                            buttons: {
                                cancel: function () {
                                },
                                delete: {
                                    text: "Delete",
                                    action: function () {
                                        $.ajax({
                                            url: "{{route("side:filter:delete")}}",
                                            dateType: "json",
                                            type: "delete",
                                            data: {
                                                "_token": CSRF_TOKEN,
                                                "id": id
                                            }
                                        }).done(function (response) {
                                            self.getFilterList();
                                        }).fail(function () {
                                            self.openSnack("Failed");
                                        })
                                    }
                                }
                            }
                        });

                    },
                    openFilterGenerator: function () {
                        let self = this;
                        $.confirm({
                            title: 'Create New Filter',
                            content: '' +
                            '<form action="" class="formName">' +
                            '<div class="form-group">' +
                            '<label>Filter name: </label>' +
                            '<input type="text" placeholder="Your name" class="name form-control" required />' +
                            '</div>' +
                            '</form>',
                            buttons: {
                                formSubmit: {
                                    text: 'Create',
                                    action: function () {
                                        var name = this.$content.find('.name').val();
                                        if (!name) {
                                            $.alert('provide a valid name');
                                            return false;
                                        }
                                        $.ajax({
                                            dataType: "json",
                                            type: "post",
                                            url: "{{route("side:filter:create")}}",
                                            data: {
                                                "_token": CSRF_TOKEN,
                                                "name": name
                                            }
                                        }).done(function () {
                                            self.getFilterList()
                                        }).fail(function () {
                                            self.openSnack("Fail. Filter not created");
                                        })
                                    }
                                },
                                cancel: function () {
                                    //close
                                },
                            },
                            onContentReady: function () {
                                // bind to events
                                var jc = this;
                                this.$content.find('form').on('submit', function (e) {
                                    // if the user submits the form by pressing enter in the field.
                                    e.preventDefault();
                                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                                });
                            }
                        });
                    },
                    openFilterDialog: function () {
                        this.getFilterList();
                        this.$refs.wordFilters.toggle();
                        this.$refs.rightSidenav.close();
                    },
                    getAlphabet: function () {
                        let self = this;
                        $.ajax({
                            url: "{{route("word:alphabet")}}",
                            dataType: "json",
                            type: "post",
                            data: {
                                "_token": CSRF_TOKEN,
                            }
                        }).done(function (response) {
                            if (response.alphabet) {
                                self.alphabet = response.alphabet
                            }
                        });
                    },
                    getFilterList: function () {
                        let self = this;
                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: "{{route('side:filter:get')}}"
                        }).done(function (response) {
                            if (response.list) {
                                self.filters = response.list;
                            }
                        })
                    },
                    //Login methods
                    openLoginDialog: function () {
                        this.registerFlag = true;
                        this.openDialog('login')
                    },
                    logIn: function () {
                        var self = this;
                        $.ajax({
                            url: "{{route('auth:login')}}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "_token": CSRF_TOKEN,
                                "email": self.user.email,
                                "password": self.user.password,
                                "word": self.word.name
                            }
                        }).done(function (response) {
                            if (response.redirect) {
                                self.user.log = true;
                                self.openSnack("You are logged in");
                                setTimeout(function () {
                                    window.location.href = response.redirect;
                                }, 2000);
                            } else {
                                self.openSnack("Invalid credentials. Please retry");
                            }
                        }).fail(function () {
                            self.openSnack("Invalid credentials. Please retry");
                        }).always(function () {

                        });
                    },
                    logout: function () {
                        var self = this;
                        $.ajax({
                            url: "{{route("auth:logout")}}",
                            type: "post",
                            data: {
                                "_token": CSRF_TOKEN,
                                "word": self.word.name
                            }
                        }).done(function (response) {
                            self.user.log = false;
                            if (response.redirect) {
                                self.openSnack("You are logged out");
                                setTimeout(function () {
                                    document.location.href = response.redirect
                                }, 2000);
                            }
                        })
                    },
                    //user information methods
                    openRegister: function (id) {
                        var self = this;
                        $.ajax({
                            url: "{{route("side:user")}}",
                            data: {
                                "_token": CSRF_TOKEN,
                                "id": id,
                            },
                            dataType: "json",
                            type: "post"
                        }).done(function (response) {
                            if (response.user) {
                                self.registerFlag = true;
                                self.$refs['register'].openRegistration(id, response.user);
                            }
                        });
                    },
                    getUserInformation: function (id) {
                        var self = this;
                        $.ajax({
                            url: "{{route("side:user")}}",
                            data: {
                                "_token": CSRF_TOKEN,
                                "id": id,
                            },
                            dataType: "json",
                            type: "post"
                        }).done(function (response) {
                            if (response.user) {
                                self.viewUser = response.user;
                                self.openDialog('userDialog');
                            }
                        })

                    },
                    //word control
                    editWord: function () {
                        this.dialog.name = this.word.name;
                        this.dialog.content = this.word.description;
                        this.dialog.id = this.word.id;
                        this.dialog.tags = this.word.tags;
                        this.dialog.filters = this.word.filters_id;
                        this.openDialog("editWord");
                    },
                    getOptions: function (search, loading) {
                        if (search.length <= 2) {
                            return false;
                        }
                        loading(true);
                        var self = this;
                        $.ajax({
                            type: "post",
                            url: "{{route("word:find")}}",
                            data: {
                                "_token": CSRF_TOKEN,
                                "word": search,
                            },
                            dataType: "json"
                        }).done(function (response) {
                            self.options = response.words;
                            loading(false)
                        }).fail(function () {
                            self.openSnack(FailSearch);
                            loading(false)
                        });
                    },
                    hide: function (item) {
                        var self = this;
                        $.ajax({
                            url: "{{route("admin:active")}}",
                            type: "post",
                            data: {
                                "_token": CSRF_TOKEN,
                                "id": item.id,
                            }
                        }).done(function (response) {
                            if (response.status !== 'undefined') {
                                item.status = response.status;
                                if (item.id == self.word.id) {
                                    self.word.status = response.status;
                                }
                            }
                        }).fail(function () {
                            return self.openSnack("Server Error.");
                        }).always(function () {
                        });
                    },
                    deleteComment: function (id) {
                        var self = this;
                        $.confirm({
                            title: 'Delete',
                            content: 'Are you sure?',
                            type: 'red',
                            autoClose: 'cancel|8000',
                            buttons: {
                                ok: {
                                    text: "delete",
                                    btnClass: 'md-button md-raised md-theme-default md-accent',
                                    keys: ['enter'],
                                    action: function () {
                                        $.ajax({
                                            url: "{{route("word:comment:delete")}}",
                                            type: "POST",
                                            dataType: "json",
                                            data: {
                                                "_token": CSRF_TOKEN,
                                                "comment_id": id
                                            }
                                        }).done(function (response) {
                                            /** TODO: process comments **/
                                            self.word.comments = response.comments;
                                            var $finder = self.list.find(function (item) {
                                                return item.name === self.word.name;
                                            });
                                            $finder.comments = response.comments.length;
                                        });
                                    }
                                },
                                cancel: {
                                    text: "Cancel",
                                    btnClass: 'md-button md-raised md-theme-default',
                                }

                            }
                        });
                    },
                    createWord: function () {
                        this.dialog.title = "Create Word";
                        this.dialog.name = "";
                        this.dialog.content = "";
                        this.dialog.id = "";
                        this.dialog.tags = [];
                        this.dialog.filters = [];
                        this.openDialog("editWord");
                    },
                    saveWord: function (id) {
                        var self = this;
                        $.ajax({
                            url: "{{route("admin:create")}}",
                            type: "POST",
                            data: {
                                "_token": CSRF_TOKEN,
                                "id": id,
                                "name": self.dialog.name,
                                "description": self.dialog.content,
                                "tags": self.dialog.tags.map(function (tag) {
                                    return tag.name;
                                }),
                                "filters": self.dialog.filters,
                            },
                            dataType: "json"
                        }).done(function (response) {
                            if (response.word) {
                                self.selectWord(response.word.name);
                            }
                            self.$refs.editWord.close();
                        }).fail(function (response) {
                            return self.openSnack("Check your data");
                        });

                    },
                    //userControl
                    loadUsersList: function () {
                        let self = this;
                        $.ajax({
                            type: "post",
                            dataType: "json",
                            url: '{{route("side:users:api")}}',
                            data: {
                                "_token": CSRF_TOKEN,
                                "limit": self.pagesLimit,
                                "page": self.usersPage,
                                "query": self.userListFinder,
                            }
                        }).done(function (response) {
                            self.users = response.users;
                            self.usersPages = Math.ceil(response.count / self.pagesLimit);
                        });
                    },
                    changeUsersPagination: function () {
                        this.usersPage = 1;
                        this.loadUsersList();
                    },
                    nextUsersPage: function () {
                        if (this.usersPage < this.usersPages) {
                            this.usersPage++;
                            this.loadUsersList();
                        }
                    },
                    prevUsersPage: function () {
                        if (this.usersPage > 1) {
                            this.usersPage--;
                            this.loadUsersList();
                        }
                    },
                    toggleUserAdminNav: function () {
                        this.loadUsersList();
                        this.$refs.wordFilters.close();
                        this.$refs.rightSidenav.toggle();
                    },
                    closeRightSidenav: function () {
                        this.$refs.rightSidenav.close();
                    },
                    closeWordFilters: function () {
                        this.$refs.wordFilters.close();
                    },
                    superUser: function (user) {
                        let self = this;
                        $.ajax({
                            type: "put",
                            dataType: "json",
                            url: '{{route("side:user:super")}}',
                            data: {
                                user_id: user.id,
                                "_token": CSRF_TOKEN,
                            }
                        }).done(function (response) {
                            if (response.user) {
                                user.role_id = response.user.role_id;
                            }
                        }).fail(function (response) {
                            return self.openSnack("Server Error.");
                        });
                    },

                    verifyUser: function (user, role) {
                        let self = this;
                        $.ajax({
                            type: "put",
                            dataType: "json",
                            url: '{{route("side:user:confirm")}}',
                            data: {
                                user_id: user.id,
                                role_id: role,
                                "_token": CSRF_TOKEN,
                            }
                        }).done(function (response) {
                            if (response.user) {
                                user.role_id = response.user.role_id;
                                user.role = response.user.role;
                            }
                            console.log(user);
                        }).fail(function (response) {
                            return self.openSnack("Server Error.");
                        });
                    },
                    banUser: function (user) {
                        let self = this;
                        $.ajax({
                            type: "put",
                            dataType: "json",
                            url: '{{route("side:user:ban")}}',
                            data: {
                                user_id: user.id,
                                "_token": CSRF_TOKEN,
                            }
                        }).done(function (response) {
                            if (response.user) {
                                user.role_id = response.user.role_id;
                            }
                        }).fail(function (response) {
                            return self.openSnack("Server Error.");
                        });
                    },
                    securityAuth: function (user) {
                        let self = this;
                        $.ajax({
                            type: "post",
                            dataType: "json",
                            url: '{{route("side:security:auth")}}',
                            data: {
                                user_id: user.id,
                                "_token": CSRF_TOKEN
                            }
                        }).done(function (response) {
                            if (response.redirect) {
                                self.openSnack("You are logged in");
                                setTimeout(function () {
                                    window.location.href = response.redirect;
                                }, 2000);
                            }
                        }).fail(function (response) {
                            return self.openSnack("Server Error.");
                        });
                    },
                    //view methods
                    changeMode: function () {
                        this.modeView = !this.modeView;
                    },
                    toggleLeftSidenav: function () {
                        this.$refs.leftSidenav.toggle();
                    },
                    //user actions
                    like: function (word) {
                        var self = this,
                            url = "{{route("word:like")}}";
                        if (word.like) {
                            url = "{{route("word:dislike")}}"
                        }
                        $.ajax({
                            url: url,
                            type: "post",
                            dataType: "json",
                            data: {
                                "_token": CSRF_TOKEN,
                                "word_id": word.id,
                                "word": word.name
                            }
                        }).done(function (response) {
                            if (response.word) {
                                let $finder = self.list.find(function (item) {
                                    return item.name === word.name;
                                });
                                $finder.likes = response.word.likes.length;
                                let comments = self.word.comments;
                                self.word = response.word;
                                self.word.comments = comments;
                            } else {
                                let $finder = self.list.find(function (item) {
                                    return item.name === word.name;
                                });
                                $finder.likes = response.likes.length;
                                self.word.likes = response.likes;
                                self.word.people = response.people;
                                self.word.like = response.like;
                            }

                        }).fail(function (response) {
                            if (response.status === 401) {
                                $.alert("Please Log in");
                                self.refs['login'].open();
                            }
                        });
                    },
                    selectLetter: function (letter) {
                        this.word = '';
                        this.letter = letter;
                        this.loadList();
                    },
                    commentAction: function () {
                        var self = this,
                            comment = self.comment;
                        if (!self.comment) {
                            self.openSnack("U can't add empty comment");
                            return false;
                        }
                        self.comment = null;
                        $.ajax({
                            url: "{{route("word:comment")}}",
                            type: "PUT",
                            dataType: "json",
                            data: {
                                "_token": CSRF_TOKEN,
                                "content": comment,
                                "word_id": self.word.id
                            }
                        }).done(function (response) {
                            if (response.comments) {
                                self.comment = '';
                                self.word.comments = response.comments;
                                var $finder = self.list.find(function (item) {
                                    return item.name === self.word.name;
                                });
                                $finder.comments = response.comments.length;
                            }
                        }).fail(function () {
                            self.comment = '';
                            self.openSnack("U can't add empty comment");
                            return false;
                        });
                    },
                    //words loader
                    loadList: function () {
                        var self = this;
                        self.word = '';
                        $.ajax({
                            url: "{{route("word:random")}}",
                            type: "post",
                            dataType: "json",
                            data: {
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        }).done(function (response) {
                            self.list = response.collection;
                            if (response.word) {
                                self.word = response.word;
                                history.pushState(null, null, '/' + self.word.name);
                                self.loadComments();
                            }
                        })
                    },
                    selectWord: function (word) {
                        var self = this;
                        self.word = "";
                        self.comment = "";
                        $.ajax({
                            url: "{{route("word:get")}}",
                            type: "post",
                            dataType: "json",
                            data: {
                                "word": word,
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        }).done(function (response) {
                            if (response.word) {
                                self.word = response.word;
                                self.list = response.collection;
                                history.pushState(null, null, '/' + self.word.name);
                                self.loadComments();
                            } else {
                                self.justGiveList();
                                return self.openSnack(FailParemeters);
                            }
                        }).fail(function () {
                            self.justGiveList();
                            self.openSnack(FailParemeters);
                        })
                    },
                    selectRelate: function (word) {
                        var self = this;
                        self.letter = '';
                        self.selectWord(word);
                    },
                    //loaders
                    updateList: function () {
                        this.loadList();
                    },
                    justGiveList: function () {
                        var self = this;
                        self.word = "";
                        self.comment = "";
                        $.ajax({
                            url: "{{route("word:list")}}",
                            type: "post",
                            dataType: "json",
                            data: {
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        }).done(function (response) {
                            self.openDialog('notFound');
                            self.list = response.collection;
                        }).fail(function () {
                            self.openSnack("Sorry, but service not available. Please connect administrator");
                        })
                    },
                    loadComments: function () {
                        var self = this;
                        $.ajax({
                            url: "{{route("word:comments")}}",
                            type: "post",
                            dataType: "json",
                            data: {
                                "_token": CSRF_TOKEN,
                                "id": self.word.id,
                                "name": self.word.name
                            }
                        }).done(function (response) {
                            if (response.comments) {
                                self.word.comments = response.comments;
                            }
                        })
                    },
                    //finders
                    submit: function () {
                        this.findWord();
                    },
                    findWord: function () {
                        var self = this;
                        self.letter = '';
                        if (!self.finder) {
                            return self.openSnack(FailParemeters);
                        }
                        $.ajax({
                            url: "{{route("word:find")}}",
                            type: "post",
                            dataType: "json",
                            data: {
                                "word": self.finder,
                                "_token": CSRF_TOKEN,
                                "letter": self.letter
                            }
                        }).done(function (response) {
                            if (response.word) {
                                if (response.words.length == 1) {
                                    self.selectWord(response.word.name);
                                } else {
                                    self.openSnack("Found [" + response.words.length + "]. You can select one of them.");
                                    self.word = response.word;
                                    self.list = response.words;
                                    history.pushState(null, null, '/' + self.word.name);
                                    self.loadComments();
                                }
                            } else {
                                self.openSnack(FailSearch)
                            }
                        }).fail(function () {
                            self.openDialog('notFound')
                        })
                    },
                    //helper methods
                    convertDate: function (date) {
                        return moment(date).format("dddd, MMMM Do YYYY");
                    },
                    fromDate: function (date) {
                        return moment(date).fromNow();
                    },
                    openDialog: function (ref) {
                        this.$refs[ref].open();
                    },
                    closeDialog: function (ref) {
                        this.$refs[ref].close();
                    },
                    openSnack: function (content) {
                        this.exceptionObject.content = content;
                        this.$refs.snackbar.open();
                    },
                    closeSnack: function () {
                        this.$refs.snackbar.close();
                        this.findWord();
                    },
                    checkWord: function (item) {
                        return item === this.word.name;
                    },
                }
            });
        });
    </script>
@endsection