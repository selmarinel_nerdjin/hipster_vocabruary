<md-dialog ref="notFound">
    <md-dialog-title>@{{ alertObject.title }}</md-dialog-title>
    <md-dialog-content>
        @{{ alertObject.text }}
    </md-dialog-content>
    <md-dialog-actions>
        <md-button class="md-primary" @click="closeDialog('notFound')">Ok</md-button>
    </md-dialog-actions>
</md-dialog>