<md-dialog md-open-from="#custom" md-close-to="#custom" ref="login" v-if="!user.log">
    <md-layout md-align="center" md-gutter="16">
        <md-card class="auth">
            <md-card-header class="hidden-xs">
                <span class="md-display-1">@lang('home.enter')</span>
                <span class="md-subheading">@lang('home.to_site')</span>
            </md-card-header>

            <md-card-content class="hidden-xs">
                <md-input-container>
                    <label>@lang('home.email')</label>
                    <md-input v-model="user.email"></md-input>
                </md-input-container>

                <md-input-container md-has-password>
                    <label>@lang('home.password')</label>
                    <md-input type="password" v-model="user.password"></md-input>
                </md-input-container>

                <div class="md-subhead">@lang('home.log_social')</div>

                <a href="{{route('login:facebook')}}">
                    <md-button class="md-fab md-primary">
                        <md-icon md-iconset="fa fa-facebook"></md-icon>
                    </md-button>
                </a>
            </md-card-content>
            <md-card-content class="hidden-lg hidden-md hidden-sm">
                <div class="text-center">
                    <span class="md-display-1">@lang('home.enter')</span>
                    <span class="md-subheading">@lang('home.to_site')</span>
                </div>
                <div class="text-center">
                    <a href="{{route('login:facebook')}}">
                        <md-button class="md-fab md-primary">
                            <md-icon md-iconset="fa fa-facebook"></md-icon>
                        </md-button>
                    </a>
                </div>
                <div class="text-center" style="font-size: 12px">
                    @lang('home.more_login_options')
                </div>
            </md-card-content>
            <md-card-actions class="hidden-xs">
                <md-button class="md-raised" @click="closeDialog('login')">@lang('home.cancel')</md-button>
                <md-button class="md-raised md-primary" @click="logIn">@lang('home.login')</md-button>
            </md-card-actions>
        </md-card>
    </md-layout>
</md-dialog>