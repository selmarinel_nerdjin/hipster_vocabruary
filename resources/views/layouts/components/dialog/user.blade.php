<md-dialog md-open-from="#app" md-close-to="#app" ref="userDialog">
    <md-dialog-title>@lang('home.about_user'):</md-dialog-title>
    <md-dialog-content v-if="viewUser">
        <md-avatar class="md-large">
            <img v-bind:src="viewUser.cover">
        </md-avatar>
        <div class="md-title">@{{ viewUser.name }}</div>
        <div class="md-subhead">@{{ viewUser.email }}</div>
        <span>@{{ convertDate(viewUser.created) }}</span>

        <md-card-content v-if="viewUser.details">
            <md-list>
                <md-list-item>
                    <b>@lang('home.age')</b>
                    <span>@{{ viewUser.details.age }}</span>
                </md-list-item>

                <md-list-item>
                    <b>@lang('home.gender')</b>
                    <span>@{{ viewUser.details.gender }}</span>
                </md-list-item>
                <md-list-item>
                    <b>@lang('home.address')</b>
                    <span>@{{ viewUser.details.locale }}</span>
                </md-list-item>
            </md-list>
        </md-card-content>

    </md-dialog-content>
    <md-dialog-actions>
        <md-button class="md-primary" @click="closeDialog('userDialog')">@lang('home.ok')</md-button>
    </md-dialog-actions>
</md-dialog>