<md-dialog ref="editWord" class="word-edit">
    <md-dialog-title>@{{ dialog.title }}</md-dialog-title>
    <md-dialog-content class="word-edit-content">
        <section class="title-section general-section">
            <md-icon class="md-primary">book</md-icon>
            General information
        </section>
        <md-input-container>
            <label>Word:</label>
            <md-input v-model="dialog.name"></md-input>
        </md-input-container>
        <v-select multiple
                  :debounce="250"
                  :on-search="getOptions"
                  :options="options"
                  placeholder="Select seems words"
                  label="name"
                  v-model="dialog.tags"
                  :value.sync="word.tags"
        >
        </v-select>
        <vue-editor
                placeholder="Word Description"
                v-model="dialog.content"
        >
       </vue-editor>
        <section class="title-section filter-section">
            <md-icon class="md-primary">filter_list</md-icon>
            Filters
        </section>
        <div>
            <md-switch v-model="dialog.filters[key]" v-for="filter,key in filters">
                @{{ filter }}
            </md-switch>
        </div>
    </md-dialog-content>

    <md-dialog-actions>
        <md-button class="md-primary" @click="closeDialog('editWord')">Cancel</md-button>
        <md-button class="md-primary" @click="saveWord(dialog.id)">Ok</md-button>
    </md-dialog-actions>
</md-dialog>