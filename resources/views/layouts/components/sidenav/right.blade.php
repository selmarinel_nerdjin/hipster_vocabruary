@if(Auth::user() && Auth::user()->isSuperUser())
    <md-sidenav class="md-right md-fixed" ref="rightSidenav" md-swipeable>
        <md-dialog-title>
            <md-button class="md-mini" @click="closeRightSidenav">
                <md-icon>
                    keyboard_arrow_left
                </md-icon>
                Administate Users
            </md-button>
        </md-dialog-title>
        <md-dialog-content>
            <md-list class="custom-list md-triple-line">
                <md-list-item v-for="user in users" :key="user.id">
                    <md-avatar>
                        <img :src="user.cover" alt="People">
                    </md-avatar>
                    <div class="md-list-text-container">
                        <span>@{{user.name}}</span>
                        <span>@{{user.email}}</span>
                        <span>@{{ user.role }}
                            <md-icon>@{{ (user.role_id != 3)?'verified_user':'' }}</md-icon></span>
                    </div>
                    <section>
                        <md-button
                                @click="superUser(user,3)"
                                v-bind:class="{hidden: user.role_id == '1' || user.role_id == '3'}"
                                class="md-icon-button md-list-action md-primary">
                            <md-icon class="md-primary">security</md-icon>
                            <md-tooltip md-direction="bottom">SET Superuser</md-tooltip>
                        </md-button>

                        <md-button
                                @click="verifyUser(user,4)"
                                class="md-icon-button md-list-action md-moderator"
                                v-bind:class="{hidden:user.role_id == '4' || user.role_id == '3' || user.role_id == '1'}"
                        >
                            <md-icon class="md-warn">perm_identity</md-icon>
                            <md-tooltip md-direction="bottom">SET Moderator</md-tooltip>
                        </md-button>

                        <md-button
                                @click="verifyUser(user,5)"
                                class="md-icon-button md-list-action md-editor"
                                v-bind:class="{hidden:user.role_id == '5' || user.role_id == '3' || user.role_id == '1'}"
                        >
                            <md-icon class="md-default">create</md-icon>
                            <md-tooltip md-direction="bottom">SET Reporter</md-tooltip>
                        </md-button>

                        <md-button
                                @click="verifyUser(user,6)"
                                class="md-icon-button md-list-action md-success"
                                v-bind:class="{hidden:user.role_id != '1' || user.role_id == '3'}"
                        >
                            <md-icon class="md-default">verified_user</md-icon>
                            <md-tooltip md-direction="bottom">SET Verified</md-tooltip>
                        </md-button>

                        <md-button
                                @click="banUser(user)"
                                class="md-icon-button md-list-action md-ban"
                                style="margin-left: 16px"
                                v-bind:class="{hidden:user.role_id == 1 || user.role_id == 3}"
                        >
                            <md-icon class="md-default">block</md-icon>
                            <md-tooltip md-direction="bottom">Ban</md-tooltip>
                        </md-button>
                        <md-button
                                @click="securityAuth(user)"
                                class="">
                            Auth
                        </md-button>
                    </section>
                    <md-divider class="md-inset"></md-divider>
                </md-list-item>
            </md-list>
        </md-dialog-content>
        <md-dialog-actions>
            <form class="navbar-form form-inline" role="search" novalidate @submit.stop.prevent="">
                <md-input-container md-inline>
                    <md-input type="text" style="width:100px" v-model="userListFinder"
                              @change="usersPage = 1"></md-input>
                    <md-button @click="loadUsersList">
                        <md-icon>search</md-icon>
                    </md-button>
                </md-input-container>
            </form>
            <span style="width:50px">
                <md-select v-model="pagesLimit" @change="changeUsersPagination()" style="min-width: 0!important;">
                <md-option v-for="lim in [5,10,20,50]" :value="lim">@{{ lim }}</md-option>
            </md-select>
            </span>
            <md-button @click="prevUsersPage">
                <md-icon>arrow_back</md-icon>
                Back
            </md-button>
            <b>@{{ usersPage }}</b>
            <md-button @click="nextUsersPage">Next
                <md-icon>arrow_forward</md-icon>
            </md-button>
        </md-dialog-actions>
    </md-sidenav>
@endif
