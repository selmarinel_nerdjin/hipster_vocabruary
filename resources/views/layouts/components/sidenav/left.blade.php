<md-sidenav class="md-left" ref="leftSidenav">
    <md-toolbar>
        <div class="md-toolbar-container">
            <h3 class="md-title">
                <md-avatar>
                    <img src="{{asset("img/em.png")}}" alt="Avatar">
                </md-avatar>
                Slangshot
            </h3>
        </div>
    </md-toolbar>
    <section class="section_user">
        <div class="text-center">
            @if(!Auth::guest())
                <md-avatar class="md-large xs-large user-avatar">
                    <img src="{{Auth::user()->getCover()}}" class="img-responsive img-circle"
                         style="margin: 0 auto;">
                </md-avatar>
            @else
                <md-button class="md-raised md-dense" @click="openLoginDialog()">@lang('home.login')</md-button>
            @endif
        </div>
    </section>
    <section>
        @if(!Auth::guest())
            <md-list>
                <md-subheader>{{Auth::user()->name}}</md-subheader>
                <md-list-item>
                    <md-icon>account_box</md-icon>
                    <span>@lang('home.account')</span>
                    <md-list-expand>
                        <md-list>
                            <md-list-item
                                    class="md-inset"
                                    @click="getUserInformation({{Auth::user()->id}})">
                                @lang('home.about_self')
                                <md-icon>info_outline</md-icon>
                            </md-list-item>
                            <md-list-item
                                    class="md-inset"
                                    @click="openRegister({{Auth::user()->id}})">
                                @lang('home.modify_info')
                                <md-icon>person_outline</md-icon>
                            </md-list-item>
                            <md-list-item
                                    class="md-inset"
                                    @click="logout">
                                @lang('home.logout')
                                <md-icon>exit_to_app</md-icon>
                            </md-list-item>
                        </md-list>
                    </md-list-expand>
                </md-list-item>
                @if(Auth::user()->isSuperUser())
                    <md-list-item @click="toggleUserAdminNav">
                        Administrate users
                    </md-list-item>
                    <md-list-item @click="openFilterDialog">
                        Word's Filters
                    </md-list-item>
                @endif
            </md-list>
        @endif
        <md-list>
            <md-subheader>Language</md-subheader>
            <md-list-item>
                <md-button href="{{route('lang',['locale'=>'en'])}}">
                    <md-avatar>
                        <img src="{{asset('img/en.png')}}" alt="English">
                    </md-avatar>
                    EN
                </md-button>
                <md-button href="{{route('lang',['locale'=>'ua'])}}">
                    <md-avatar>
                        <img src="{{asset('img/ua.png')}}" alt="Ukrainian">
                    </md-avatar>
                    UA
                </md-button>
            </md-list-item>
        </md-list>
    </section>
    <div class="hidden-lg hidden-md hidden-sm">
        <md-whiteframe md-tag="section">
            <md-subheader>
                <b class="big">@lang('home.word_list')</b>
                <md-button class="md-icon-button holder" @click="updateList()">
                    <md-icon>cached</md-icon>
                </md-button>

                <md-button class="md-icon-button" v-if="admin" @click="createWord()">
                    <md-icon>library_add</md-icon>
                </md-button>
                <md-button class="md-icon-button" @click="changeMode">
                    <md-icon>@{{(modeView)?'view_headline':'view_agenda'}}</md-icon>
                </md-button>
            </md-subheader>
            <md-list class="custom-list" v-bind:class="{'md-triple-line md-dense':modeView}">
                <md-list-item v-for="item,key in sortList"
                              @click="selectWord(item.name)"
                              v-bind:class="{active:checkWord(item.name)}">
                    <div class="md-list-text-container">
                        <span>@{{ item.name }}</span>
                        <span v-if="modeView" v-html="convertDate(item.created_at)"></span>
                        <p v-if="modeView">
                            <md-icon class="md-like">favorite</md-icon>@{{ item.likes }}
                            <md-icon class="md-comment">comment</md-icon>@{{ item.comments }}
                            <md-icon class="md-links">link</md-icon>@{{ item.tags }}
                        </p>
                    </div>
                    <md-divider class="md-inset" v-if="modeView"></md-divider>
                    <md-button
                            class="md-icon-button md-list-action"
                            v-if="admin"
                            @click="hide(item)"
                            v-bind:class="{disable:!item.status,submitted:(item.status === {{\App\Models\Word::SUBMITTED}})}">
                        <md-icon class="md-primary">
                            @{{ (item.status === 1)?"visibility_off":"visibility" }}
                        </md-icon>
                    </md-button>
                </md-list-item>
            </md-list>
            <md-bottom-bar md-shift>
                <md-bottom-bar-item md-icon="info" md-active>
                    @{{ wordsCount }}
                </md-bottom-bar-item>
            </md-bottom-bar>
        </md-whiteframe>
    </div>
</md-sidenav>