<md-sidenav class="md-right md-fixed" ref="wordFilters" md-swipeable style="width: 75vw">
    <md-dialog-title>
        <md-button class="md-mini" @click="closeWordFilters">
            <md-icon>
                keyboard_arrow_left
            </md-icon>
            Administrate Word's Filters
        </md-button>
    </md-dialog-title>
    <md-dialog-content>
        <div class="phone-viewport">
            <md-list>
                <md-subheader>
                    Filters
                    <md-button class="md-icon-button" @click="openFilterGenerator()">
                        <md-icon>note_add</md-icon>
                    </md-button>
                </md-subheader>
                <md-list-item v-for="filter,key in filters">
                    <md-input-container>
                        <md-input v-model="filter"></md-input>
                    </md-input-container>
                    <md-button class="md-icon-button md-list-action" @click="editFilter(key,filter)">
                        <md-icon class="md-primary">save</md-icon>
                    </md-button>
                    <md-button  class="md-icon-button md-list-action disable" @click="deleteFilter(key)">
                        <md-icon class="md-primary">
                            delete
                        </md-icon>
                    </md-button>
                </md-list-item>
            </md-list>
        </div>
    </md-dialog-content>
</md-sidenav>