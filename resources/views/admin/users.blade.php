@extends('admin.dashboard')
@section('content')
    <h1><b>Dashboard ... </b>Coming soon</h1>
    <div class="col-lg-12 col-xs-12">
        <users-dashboard
                :url_users="'{{route("side:users")}}'"
                :save_url="'{{route("dashboard:user:put")}}'"
                :roles_url="'{{route("dashboard:roles:get")}}'"
                :auth_url="'{{route("side:security:auth")}}'"
        >
        </users-dashboard>
    </div>
@endsection