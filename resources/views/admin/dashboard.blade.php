<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin::@yield("title","Dashboard")</title>
    {{--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <!-- Styles -->
    <style>
        [v-cloak] {
            display: none;
        }
    </style>
    @yield("css")
</head>
<body>
<div id="app" v-cloak class="wrapper">

    <div class="sidebar" data-color="purple" data-image="">
        <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
        -->

        <div class="logo">
            <a href="{{route("home")}}" class="simple-text">
                HIPSTER VOCABRUARY
            </a>
        </div>

        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="active">
                    <a href="#">
                        <md-icon>dashboard</md-icon>
                        <p>Dashboard</p>
                    </a>
                </li>
                {{--<li>--}}
                    {{--<a href="{{route("dashboard:words")}}">--}}
                        {{--<md-icon>list</md-icon>--}}
                        {{--<p>Words</p>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li>
                    <a href="{{route("dashboard:users")}}">
                        <md-icon>person</md-icon>
                        <p>Users</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Material Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if(!Auth::guest())
                            <li>
                                <md-menu>
                                    <md-button md-menu-trigger>
                                        <md-avatar>
                                            <img src="{{Auth::user()->getCover()}}" alt="People">
                                        </md-avatar>
                                        {{Auth::user()->name}}
                                    </md-button>

                                    <md-menu-content>
                                        <md-menu-item @click="logout">
                                            Logout
                                            <md-icon>exit_to_app</md-icon>
                                        </md-menu-item>
                                    </md-menu-content>
                                </md-menu>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/admin.js') }}"></script>
@yield('scripts')
<script>
    var app = new Vue({
        el: '#app',
        methods: {
            toggleLeftSidenav() {
                this.$refs.leftSidenav.toggle();
            },
            logout: function () {
                $.ajax({
                    url: "{{route("auth:logout")}}",
                    type: "post",
                    data: {
                        "_token": CSRF_TOKEN,
                    }
                }).done(function (response) {
                    $.notify({
                        message: 'Logged Out'
                    }, {
                        type: 'info'
                    });
                    if (response.redirect) {
                        setTimeout(function () {
                            document.location.href = response.redirect
                        }, 1000);
                    }
                })
            },
        }
    })
</script>
</body>
</html>