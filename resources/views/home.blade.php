@extends('layouts.landing')
@section('css')
    <link href="https://fonts.googleapis.com/css?family=Exo+2:100" rel="stylesheet">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
@endsection
@section('content')
    @include("v2.components.login")
    <section id="main">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <span class="navbar-brand" href="#">
                <span class="title">
                    Slangshot
                </span>
            </span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="mr-auto">

                </div>
                <span class="navbar-text my-2 my-lg-0">
                    @if(Auth::guest())
                        <button type="button"
                                @click="openLoginDialog"
                                class="btn btn-outline-light">
                            @lang('home.login')
                        </button>
                    @else
                        <div class="navbar-text">
                            <img src="{{Auth::user()->getCover()}}" class="img-fluid" width="30" height="30">
                            Welcome, {{ Auth::user()->name }}. &nbsp;
                        </div>
                        <button type="button"
                                class="btn btn-outline-light"
                                @click="logout">
                            @lang('home.logout')
                        </button>
                    @endif
                </span>
            </div>
        </nav>
        <div class="page-title">
            <form @submit.prevent="find">
                <h2 class="small-title">Знайдiть те саме слово</h2>
                <div v-if="!searching">
                    <input type="text" name="query" v-model="query" class="word-finder" placeholder="Введiть слово">
                </div>
                <div class="progress" v-if="searching">
                    <div
                            class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                            role="progressbar"
                            :aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            :style="{width:searchingPercent+'%'}"></div>
                </div>
                <button class="btn btn-dark btn-lg" type="button" @click="find">Знайти</button>
            </form>
        </div>

    </section>
    <section class="container" id="content" style="display: none">
        <div class="row row-card" v-if="word">
            <section class="col-8">
                <section class="row">
                    <h2 class="card-title word-title">
                        @{{ word.name }}
                        <a class="btn btn-outline-success" :href="'/' + word.name">
                            <i class="fa fa-external-link-square" aria-hidden="true"></i>
                        </a>
                    </h2>
                    <div class="card-body">
                        <p class="card-text roboto" v-html="word.description"></p>
                        <div>
                            <span><i class="fa fa-link" aria-hidden="true"></i>@{{ word.tags }}</span>
                            <span><i class="fa fa-heart" aria-hidden="true"></i>@{{ word.likes }}</span>
                        </div>
                    </div>
                </section>
                <section class="row">
                    <div class="col-12">
                        <h5 class="card-title roboto">@lang('home.comments'):</h5>
                        @if(!Auth::guest())
                            <form novalidate class="md-layout-row md-gutter" @submit.stop.prevent="commentAction">
                                <md-field>
                                    <label>@lang('home.comment')</label>
                                    <md-textarea maxlength="255" v-model="comment"></md-textarea>
                                </md-field>
                                <md-button class="md-raised md-mini" type="commentAction" v-show="comment">
                                    <span>@lang('home.send')</span>
                                    <md-icon>send</md-icon>
                                </md-button>
                            </form>
                        @else
                            <div class="text-center">
                                @lang('home.notice_register')
                                <div>
                                    <md-button class="md-dense"
                                               @click="openLoginDialog">@lang('home.login')</md-button>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-12">
                        <div class="row justify-content-center">
                            <div class="col">
                                <div class="list-group" v-if="word.comments && word.comments.length"
                                     style="margin-top: 20px">
                                    <div v-for="item,key in word.comments"
                                         class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1">
                                                <md-avatar>
                                                    <img :src="item.user.cover" :alt="item.user.name">
                                                </md-avatar>
                                                @{{ item.user.name }}
                                            </h5>
                                            <small>@{{ processData(item.created_at) }}</small>
                                        </div>
                                        <p class="mb-1" v-html="item.content"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="clear-fix" style="padding: 10px"></div>
                <section class="row" v-if="word.tags && word.tags.length">
                    <h5 class="roboto">Синонiми:</h5>
                    <div class="row row-card" v-for="relate in synonim in word.tags">
                        <div class="card-body">
                            <h5 class="card-title">@{{ relate.name }}</h5>
                            <p class="card-text" v-html="relate.description"></p>
                            <button type="button" class="btn btn-outline-dark btn-sm"
                                    @click="selectWord(relate.name)">
                                Show detailed
                            </button>
                            <a class="btn btn-outline-success" :href="'/' + relate.name">
                                <i class="fa fa-external-link-square" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </section>
                <section class="row">
                    <div class="col">
                        <h5 class="roboto">Бiльше слiв:</h5>
                        <div class="row row-card" v-for="relate in relatedList">
                            <div class="card-body">
                                <h5 class="card-title">@{{ relate.name }}</h5>
                                <p class="card-text" v-html="relate.description"></p>
                                <button type="button" class="btn btn-outline-dark btn-sm"
                                        @click="selectWord(relate.name)">
                                    Show detailed
                                </button>
                                <a class="btn btn-outline-success" :href="'/' + relate.name">
                                    <i class="fa fa-external-link-square" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

            </section>
            <section class="col-4">
                <section>
                    <h5 class="roboto word-title">New Random Words</h5>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center"
                            v-for="word in newest">
                            @{{ word.name }}
                            <div>
                                <button class="btn btn-dark" @click="selectWord(word.name)">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                                <a class="btn btn-outline-success" :href="'/' + word.name">
                                    <i class="fa fa-external-link-square" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </section>
                <section>
                    <h5 class="roboto word-title">The Most Popular</h5>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center"
                            v-for="word in newest">
                            @{{ word.name }}
                            <div>
                                <button class="btn btn-dark" @click="selectWord(word.name)">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </button>
                                <a class="btn btn-outline-success" :href="'/' + word.name">
                                    <i class="fa fa-external-link-square" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </section>
            </section>
        </div>
    </section>
    <footer class="container-fluid footer">
        <div class="row">
            <div class="col-12 text-center">
                <span class="logo">SlangShot</span>
                &copy; Selmarinel 2017
            </div>
        </div>
    </footer>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            let app = new Vue(
                {
                    el: '#app',
                    data: {
                        newest: [],
                        popular: [],
                        query: '',
                        words: [],
                        searching: false,
                        searchingPercent: 0,
                        word: {},
                        comment: '',
                        user: {},
                        showLoginDialog: false,
                    },
                    computed: {
                        relatedList: function () {
                            if (this.words) {
                                this.words.splice(0, 1);
                                return this.words;
                            }
                            return [];
                        }
                    },
                    methods: {
                        getNewest: function () {
                            let self = this;
                            $.ajax({
                                url: "{{route("word:newest")}}",
                                type: "post",
                                dataType: "json",
                                data: {
                                    "_token": CSRF_TOKEN,
                                }
                            }).done(function (response) {
                                self.newest = response.collection;
                            }).fail(function () {
                                self.getNewest();
                            })
                        },
                        getPopular: function () {
                            let self = this;

                            $.ajax({
                                url: "{{route("word:popular")}}",
                                type: "post",
                                dataType: "json",
                                data: {
                                    "_token": CSRF_TOKEN,
                                }
                            }).done(function (response) {
                                self.popular = response.collection;
                            }).fail(function () {
                                self.getPopular();
                            })
                        },
                        processData: function (date) {
                            return moment(date).fromNow();
                        },
                        runSearching: function () {
                            let self = this;
                            setInterval(function () {
                                if (self.searchingPercent > 100) {
                                    self.searchingPercent = 0;
                                }
                                self.searchingPercent++;
                            }, 50);
                        },
                        selectWord: function (word) {
                            this.query = word;
                            this.find();
                            return true;
                        },
                        find: function () {
                            let self = this;
                            self.searchingPercent = 0;
                            self.searching = true;
                            self.runSearching();
                            $.ajax({
                                url: "{{route("api:find")}}",
                                type: "post",
                                dataType: "json",
                                data: {
                                    "_token": CSRF_TOKEN,
                                    "query": self.query
                                }
                            }).done(function (response) {
                                if (response.words && response.words.length) {
                                    $("#content").show();
                                    self.words = response.words;
                                    self.word = self.words[0];
                                    self.word.comments = [];
                                    self.getComments();
                                    self.getNewest();
                                    self.getPopular();
                                    setTimeout(function () {
                                        $('html, body').stop().animate({
                                            'scrollTop': $("#content").offset().top
                                        }, 100, 'linear')
                                    }, 300);

                                } else {
                                    $("#content").hide();
                                }
                            }).fail(function () {
                                $("#content").hide();
                            }).always(function () {
                                self.searching = false;
                            })
                        },
                        getComments: function () {
                            var self = this;
                            $.ajax({
                                url: "{{route("word:comments")}}",
                                type: "post",
                                dataType: "json",
                                data: {
                                    "_token": CSRF_TOKEN,
                                    "id": self.word.id,
                                }
                            }).done(function (response) {
                                if (response.comments) {
                                    self.word.comments = response.comments;
                                }
                            })
                        },
                        commentAction: function () {
                            var self = this,
                                comment = self.comment;
                            self.comment = null;
                            $.ajax({
                                url: "{{route("word:comment")}}",
                                type: "PUT",
                                dataType: "json",
                                data: {
                                    "_token": CSRF_TOKEN,
                                    "content": comment,
                                    "word_id": self.word.id
                                }
                            }).done(function (response) {
                                if (response.comments) {
                                    self.comment = '';
                                    self.word.comments = response.comments;
                                }
                            }).fail(function () {
                                self.comment = '';
                                return false;
                            });
                        },
                        openLoginDialog: function () {
                            this.showLoginDialog = true;
                        },
                        closeLoginDialog: function () {
                            this.showLoginDialog = false;
                        },
                        logout: function () {
                            let self = this;
                            axios.post("{{route("auth:logout")}}",
                                {
                                    "_token": CSRF_TOKEN,
                                    "word": self.word.name
                                }
                            ).then(function (response) {
                                if (response.data) {
                                    let redirect = response.data.redirect;
                                    setTimeout(function () {
                                        document.location.href = redirect
                                    }, 1000);
                                }
                            });
                        },
                        login: function () {
                            let self = this;
                            axios.post("{{route('auth:login')}}",
                                {
                                    "_token": CSRF_TOKEN,
                                    "email": self.user.username,
                                    "password": self.user.password,
                                    "word": self.word.name
                                }
                            ).then(function (response) {
                                if (response.data) {
                                    let redirect = response.data.redirect;
                                    if (redirect) {
                                        self.closeLoginDialog();
                                        setTimeout(function () {
                                            document.location.href = redirect
                                        }, 1000);
                                    }
                                }
                            }).catch(function (error) {

                            });
                        },
                        checkLoginState: function () {
                            let self = this;
                            FB.getLoginStatus(function (response) {
                                if (response && response.status !== 'connected') {
                                    FB.login(function (response) {
                                        self.statusChangeCallback(response);
                                    }, {scope: 'public_profile,email'});
                                } else {
                                    self.statusChangeCallback(response);
                                }
                            });
                        },
                        statusChangeCallback: function (response) {
                            let data = response.authResponse;
                            if (data && data.accessToken) {
                                axios.post("{{route("api:fb:login")}}", {
                                    "_token": CSRF_TOKEN,
                                    "token": data.accessToken
                                }).then(function (response) {
                                    let data = response.data;
                                    if (data.redirect) {
                                        window.location = data.redirect;
                                    }
                                })
                            } else {
                                let message = "Canceled";
                            }
                        },
                    }
                }
            )
        });
    </script>
@endsection