

window.$ = window.jQuery = require('jquery');
// require('bootstrap');

window.Vue = require('vue');

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/black-green-light.css';

Vue.use(VueMaterial);

window.CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

window.moment = require('moment');

window.axios = require('axios');

window.fbAsyncInit = function() {
    FB.init({
        appId: '318738131865147',
        cookie: true,
        xfbml: true,
        version: 'v2.10'
    });
};

(function(d, s, id) {
    let js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

import UsersDashboard from './admin/components/users.vue';
Vue.component('users-dashboard', UsersDashboard);

require("./admin/bootstrap-notify");
