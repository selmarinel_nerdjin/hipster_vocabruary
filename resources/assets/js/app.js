/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

let VueMaterial = require('vue-material');
require('vue-material/dist/vue-material.css');


Vue.use(VueMaterial);

Vue.material.registerTheme('default', {
    primary: 'blue-grey',
    accent: 'red',
    warn: 'deep-orange',
    background: 'white'
});

window.moment = require('moment');

Pusher.logToConsole = true;

let pusher = new Pusher('e941a23887c0003b4dc8', {
    cluster: 'eu',
    encrypted: true
});

window.channel = pusher.subscribe('my-channel');

window.CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

require("jquery-confirm/dist/jquery-confirm.min.css");
require("jquery-confirm/dist/jquery-confirm.min");

import vSelect from 'vue-select'
Vue.component('v-select', vSelect);

import RegisterForm from './components/RegisterForm.vue';
Vue.component('sn-registration', RegisterForm);

let SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

import {VueEditor} from 'vue2-editor'
Vue.component('vue-editor', VueEditor);

Vue.material.registerTheme('default', {
    primary: {
        color: 'Deep Purple',
        hue: '400'
    },
});