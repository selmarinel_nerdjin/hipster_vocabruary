window.$ = window.jQuery = require('jquery');
// require('bootstrap');

window.Vue = require('vue');

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/black-green-light.css';

Vue.use(VueMaterial);

window.CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

window.moment = require('moment');

window.axios = require('axios');

let SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

import {VueEditor} from 'vue2-editor'
Vue.component('vue-editor', VueEditor);

import vSelect from 'vue-select'
Vue.component('v-select', vSelect);

import WordTags from './components/WordTags.vue';
Vue.component('word_tags', WordTags);

window.md5 = require('md5');

window.fbAsyncInit = function() {
    FB.init({
        appId: '318738131865147',
        cookie: true,
        xfbml: true,
        version: 'v2.10'
    });
};

(function(d, s, id) {
    let js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));