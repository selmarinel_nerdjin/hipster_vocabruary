<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comment";

    protected $fillable = [
        "user_id",
        "word_id",
        "content"
    ];

    protected $hidden = [
        "deleted_at",
        "updated_at",
        "user_id",
        "word_id"
    ];

    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function word()
    {
        return $this->hasOne(Word::class, "id", "word_id");
    }

    public function likes()
    {
        return $this->belongsToMany(User::class,"comment_likes","comment_id","user_id");
    }

    public function getInfo()
    {
        $result = [
            "id" => $this->id,
            "content" => $this->content,
            "created_at" => (new \DateTime($this->created_at))->format("Y-m-d H:i:s"),
            "user" => $this->user,
            "likes" => $this->likes->map(function($user){
                /** @var User $user */
                return [
                    "id" => $user->id,
                    "name" => $user->name
                ];
            })
        ];
        $result["user"]["cover"] = $this->user->getCover();
        return $result;
    }
}
