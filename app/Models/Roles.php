<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = "roles";

    public $timestamps = false;

    protected $fillable = [
        "id",
        "name"
    ];

    const USER = 1;
    const ADMIN = 2;
    const SUPERUSER = 3;
    const MODERATOR = 4;
    const REPORTER = 5;
    const VERIFIED_USER = 6;

    public static function getDefault()
    {
        return new self(["id" => self::VERIFIED_USER, "name" => "user"]);
    }

    public function rules()
    {
        return $this->belongsToMany(Rule::class,'rules_ro_role','role_id','rule_id');
    }

    private function canSome($rule)
    {
        return !$this->rules->filter(function ($model) use ($rule) {
            return $model->id == $rule;
        })->isEmpty();
    }

    public function canSendComment()
    {
        return $this->canSome(Rule::CAN_COMMENT);
    }

    public function canAddWord()
    {
        return $this->canSome(Rule::CAN_ADD_WORD);
    }

    public function canEditWord()
    {
        return $this->canSome(Rule::CAN_EDIT_WORD);
    }

    public function canVerify()
    {
        return $this->canSome(Rule::CAN_VERIFY);
    }

    public function canRemoveComment()
    {
        return $this->canSome(Rule::CAN_REMOVE_COMMENT);
    }

    public function canCreateFiler()
    {
        return $this->canSome(Rule::CAN_CREATE_FILTER);
    }

    public function canRemoveFiler()
    {
        return $this->canSome(Rule::CAN_REMOVE_FILTER);
    }

    public function canAdministrateUser()
    {
        return $this->canSome(Rule::CAN_ADD_USER);
    }
}
