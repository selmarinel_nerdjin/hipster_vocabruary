<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleToUser extends Model
{
    protected $table = "role_to_user";

    protected $fillable = [
        "role_id",
        "user_id"
    ];

    public function role()
    {
        return $this->hasOne(Roles::class,"id","role_id");
    }


}
