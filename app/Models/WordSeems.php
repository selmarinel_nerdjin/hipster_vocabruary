<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WordSeems extends Model
{
    protected $table = "word_seems";

    protected $fillable = [
        "description",
        "word_id",
        "id"
    ];

    protected $hidden = [
        "updated_at"
    ];

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = ($value) ? $value : "";
    }
}
