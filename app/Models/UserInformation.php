<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $table = "user_information";

    protected $fillable = [
        "user_id",
        "age",
        "gender",
        "locale",
        'birth',
        "cover_id"
    ];

    protected $hidden = [
        "user_id",
        "id",
        "created_at",
        "updated_at",
        "cover_id"
    ];

    public function cover()
    {
        return $this->hasOne(File::class, "id", "cover_id");
    }

    public function getCover()
    {
        if ($this->cover) {
            return $this->cover->path;
        }
        return asset("img/user.png");
    }

    public static function getDefault()
    {
        return new self(["age" => 0, "gender" => self::GENDER_ALIEN, "locale" => '']);
    }

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';
    const GENDER_ALIEN = 'alien';

    public function getGenderAttribute()
    {
        if ($this->attributes['gender'] == 1) {
            return self::GENDER_MALE;
        }
        if ($this->attributes['gender'] == 2) {
            return self::GENDER_FEMALE;
        }
        return self::GENDER_ALIEN;
    }

    public static function processGender($gender = null)
    {
        if ($gender === UserInformation::GENDER_MALE) {
            return 1;
        }
        if ($gender === UserInformation::GENDER_FEMALE) {
            return 2;
    }
        return 0;
    }

    public static function invertProcessGender($number = null)
    {
        if($number === 2){
            return static::GENDER_FEMALE;
        }
        if($number === 1){
            return static::GENDER_MALE;
        }
        return static::GENDER_ALIEN;
    }
}