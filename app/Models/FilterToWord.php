<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FilterToWord extends Model
{
    use SoftDeletes;

    protected $table = 'word_to_filter';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "word_id", "filter_id"
    ];

    public function word()
    {
        return $this->hasOne(Word::class, "id", "word_id");
    }

    public function filter()
    {
        return $this->hasOne(Filter::class, "id", "filter_id");
    }
}
