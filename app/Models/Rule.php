<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $table = "rules";

    protected $fillable = [
        "id",
        "name",
    ];

    const CAN_COMMENT = 1;
    const CAN_ADD_WORD = 2;
    const CAN_EDIT_WORD = 3;
    const CAN_VERIFY = 4;
    const CAN_REMOVE_COMMENT = 5;
    const CAN_CREATE_FILTER = 6;
    const CAN_REMOVE_FILTER = 7;
    const CAN_ADD_USER = 8;

}