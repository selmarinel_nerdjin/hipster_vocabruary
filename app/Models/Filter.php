<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filter extends Model
{
    use SoftDeletes;

    protected $table = 'filters';

    protected $fillable = [
        "name", "value"
    ];

    protected $dates = ['deleted_at'];

    public function getInfo()
    {
        return [
            "id" => $this->id,
            "name" => $this->name
        ];
    }
}
