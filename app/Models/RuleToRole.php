<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RuleToRole extends Model
{
    protected $table = "rules_ro_role";

    protected $fillable = [
        "id",
        "rule_id",
        "role_id"
    ];
}