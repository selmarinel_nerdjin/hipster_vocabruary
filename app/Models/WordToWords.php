<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WordToWords extends Model
{
    protected $table = "word_to_words";

    protected $fillable = [
        "word_id",
        "tag_id",
        "deleted_at"
    ];

    protected $hidden = [
        "created_at",
        "deleted_at",
        "updated_at"
    ];

    public function tag(){
        return $this->hasOne(Word::class,"id","tag_id");
    }

    public static function createRelation($wordId, $tagId)
    {
        self::query()->updateOrCreate(["word_id" => $wordId, "tag_id" => $tagId],["deleted_at"=>null]);
        self::query()->updateOrCreate(["word_id" => $tagId, "tag_id" => $wordId],["deleted_at"=>null]);
    }
}
