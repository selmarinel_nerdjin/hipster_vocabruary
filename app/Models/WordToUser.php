<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class WordToUser extends Model
{
    protected $table = "word_to_user";

    protected $fillable = [
        "user_id",
        "word_id",
        "like",
        "rating"
    ];

    protected $hidden = [
        "updated_at"
    ];

    public function word()
    {
        return $this->hasOne(Word::class,"id","word_id");
    }

    public function user()
    {
        return $this->hasOne(User::class,"id","user_id");
    }
}
