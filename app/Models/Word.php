<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Word extends Model
{
    protected $table = "words";

    protected $fillable = [
        "name",
        "description",
        "letter",
        "id",
        "user_id",
        "status"
    ];

    const DISABLE = 0;
    const ACTIVE = 1;
    const SUBMITTED = 2;
    const DELETED = 3;

    public function getShortInformation()
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "letter" => $this->letter,
            "created_at" => (new \DateTime($this->created_at))->format("Y-m-d H:i:s"),
            "status" => $this->status,
            "likes" => $this->likes->count(),
            "comments" => $this->comments->count(),
            "filters" => $this->filters->count(),
            "tags" => $this->tags->count()
        ];
    }

    public function getFullInformation()
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "letter" => $this->letter,
            "created_at" => (new \DateTime($this->created_at))->format("Y-m-d H:i:s"),
            "status" => $this->status,
            "comments" => [],
            "author" => $this->getAuthor(),
            "filters" => $this->getFilterAllParameters(),
            "filters_id" => $this->getFilters(),
            "likes" => $this->likes,
            "tags" => $this->tags->map(function ($model) {
                return [
                    "id" => $model->id,
                    "name" => $model->name,
                    "status" => $model->status,
                    "description" => $model->description,
                    "likes" => count($model->likes),
                    "comments" => count($model->comments),
                    "tags" => count($model->tags),
                ];
            }),
        ];
    }

    protected $hidden = [
        "updated_at",
        "user_id"
    ];

    public function getStatusAttribute()
    {
        if (in_array($this->attributes["status"], [self::ACTIVE, self::SUBMITTED])) {
            return $this->attributes["status"];
        }
        return self::DISABLE;
    }

    public function author()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function getAuthor()
    {
        if ($this->author) {
            return $this->author;
        }
        return User::getDefault();
    }

    public function setLetterAttribute($attribute)
    {
        $this->attributes["letter"] = mb_substr($this->attributes['name'], 0, 1);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = ($value) ? $value : "";
    }

    public function seems()
    {
        return $this->hasMany(WordSeems::class, "word_id", "id");
    }

    public function getSeemsCountAttribute()
    {
        return count($this->seems);
    }

    public function scopeNearMore($query, $word)
    {
        return $query->where("name", ">", $word);
    }

    public function scopeNearLess($query, $word)
    {
        return $query->where("name", "<", $word);
    }

    public function scopeByLetter($query, $letter = null)
    {
        if ($letter) {
            return $query->where("letter", $letter);
        }
        return $query;
    }

    public function scopeActive($query, $ignore = false)
    {
        if ($ignore) {
            return $query;
        }
        return $query->where("status", self::ACTIVE);
    }

    public function scopeOwner($query, User $user = null)
    {
        if (!$user) {
            return $query;
        }
        return $query->where("user_id", $user->id);
    }

    public function scopeOrOwner($query, User $user = null)
    {
        if (!$user) {
            return $query;
        }
        return $query->orWhere("user_id", $user->id);
    }

    public function isActive()
    {
        return $this->attributes["status"] === self::ACTIVE;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, "word_id", "id")->where("deleted_at", null);
    }

    public function likes()
    {
        return $this->hasMany(WordToUser::class, "word_id", "id")->where("like", 1);
    }

    public function tags()
    {
        return $this->belongsToMany(Word::class, "word_to_words", "word_id", "tag_id")
            ->where("deleted_at", null)
            ->where("words.status", Word::ACTIVE);
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, "word_to_filter", "word_id", "filter_id")
            ->where("filters.deleted_at", null)
            ->where("word_to_filter.deleted_at", null);
    }

    public function getFilterAllParameters()
    {
        if ($this->filters && !empty($this->filters)) {
            $result = [];
            /** @var Filter $filter */
            foreach ($this->filters as $filter) {
                $result[$filter->id] = $filter->getInfo();
            }
            return $result;
        }
        return [];
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        $filters = Filter::all();
        $result = [];
        foreach ($filters as $filter) {
            $result[$filter->id] = false;
            if ($this->filters->find($filter->id)) {
                $result[$filter->id] = true;
            }
        }
        return $result;
    }
}
