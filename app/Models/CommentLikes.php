<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class CommentLikes extends Model
{
    protected $table = "comment_likes";

    protected $fillable = [
        "user_id",
        "comment_id",
        "like"
    ];

    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }
}