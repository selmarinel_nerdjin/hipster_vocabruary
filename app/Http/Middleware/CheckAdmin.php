<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if(!$request->user()){
            return redirect(route("home"));
        }
        if($request->user() && $request->user()->isAdmin()){
            return $next($request);
        }
        return abort(403);
    }
}
