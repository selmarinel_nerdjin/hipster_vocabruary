<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 25.02.2018
 * Time: 15:42
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;

class RedirectorMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (mb_strripos($request->getUri(),'vergo.space')) {
            return redirect()->to("http://slangshot.in.ua/");
        }
        return $next($request);
    }
}