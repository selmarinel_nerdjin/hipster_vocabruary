<?php

namespace App\Http\Controllers;

use App\Bundles\User\Permissions\CanSeeWord;
use App\Models\Comment;
use App\Models\Rule;
use App\Models\Word;
use App\Services\PermissionService;
use App\Services\UserActionsService;
use App\Services\Word\Service;
use App\Services\WordBundle\WordHandle;
use Illuminate\Http\Request;
use Validator;

class WordsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAction(Request $request)
    {
        $service = new Service();
        $permissionService = new PermissionService();
        $service->setAdministrateRules(($request->user()) ?
            ($request->user()->isSuperUser() || ($permissionService->can($request->user(), Rule::CAN_EDIT_WORD))) :
            false);
        $service->setUser($request->user());
        $service->setLetter($request->input("letter"));
        $service->setWord($request->input('word'));
        return $this->getResponse($request, $service);
    }

    /**
     * @param Request $request
     * @param Service $service
     * @return \Illuminate\Http\JsonResponse
     */
    private function getResponse(Request $request, Service $service)
    {
        $userService = new UserActionsService($request->user());

        $word = $service->getWordModel();
        if ($service->hasErrors()) {
            return response()->json(["errors" => $service->getErrors()], 400);
        }
        return response()->json([
            'word' => array_merge($word->getFullInformation(),
                $userService->getLikedInformation($service->getWordModel())),
            "collection" => $service->getNearWordsByWord()->map(function ($word) {
                /** @var Word $word */
                return $word->getShortInformation();
            })
        ]);
    }

    public function loadListAction(Request $request)
    {
        $service = new Service();
        $service->setAdministrateRules(($request->user()) ? $request->user()->isAdmin() : false);
        $service->setLetter($request->input("letter"));
        $service->setUser($request->user());
        return [
            "collection" => $service->justGiveList()->map(function ($word) {
                return $word->getShortInformation();
            })
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomAction(Request $request)
    {
        $service = new Service();
        $service->setUser($request->user());
        $service->setAdministrateRules(($request->user()) ? $request->user()->isAdmin() : false);
        $service->setLetter($request->input("letter"));
        $service->getRandomWordModel();
        return $this->getResponse($request, $service);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPopularAction(Request $request)
    {
        $service = new Service();
        $service->setUser($request->user());
        $service->setAdministrateRules(($request->user()) ? $request->user()->isAdmin() : false);
        $service->setLetter($request->input("letter"));
        $collection = $service->getPopularList();
        return response()->json([
            "collection" => $collection->map(function ($word) {
                /** @var Word $word */
                return $word->getShortInformation();
            })
        ]);
    }

    public function getNewestAction(Request $request)
    {
        $service = new Service();
        $service->setUser($request->user());
        $service->setAdministrateRules(($request->user()) ? $request->user()->isAdmin() : false);
        $service->setLetter($request->input("letter"));
        $collection = $service->getNewestList();
        return response()->json([
            "collection" => $collection->map(function ($word) {
                /** @var Word $word */
                return $word->getShortInformation();
            })
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAction(Request $request)
    {
        $service = new Service();
        $service->setUser($request->user());
        $service->setAdministrateRules(($request->user()) ? $request->user()->isAdmin() : false);
        $service->setLetter(null);
        $finder = $service->findByWord($request->input("word"));
        if ($service->hasErrors()) {
            return response()->json(['error' => $service->getErrors()], 404);
        }
        $userService = new UserActionsService($request->user());
        $result = [
            'word' => array_merge($service->getWordModel()->getFullInformation(),
                $userService->getLikedInformation($service->getWordModel())),
            'words' => $finder->map(function ($word) {
                return $word->getShortInformation();
            })
        ];
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @param bool $like
     * @return \Illuminate\Http\JsonResponse
     */
    private function liked(Request $request, $like = false)
    {
        return $this->next($request, $like);
    }

    private function next(Request $request, $like)
    {
        $userActionsService = new UserActionsService($request->user());
        $gusto = $userActionsService->gusto($request->input("word"), $like);
        if ($gusto === false) {
            return response()->json(["message" => "Unauthorized"], 401);
        }
        return response()->json($gusto);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function likeAction(Request $request)
    {
        return $this->liked($request, true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dislikeAction(Request $request)
    {
        return $this->liked($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountAction(Request $request)
    {
        return response()->json(["count" => Word::query()->active()->count()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCommentsAction(Request $request)
    {
        $service = new Service();
        return response()->json(["comments" => $service->getComments($request->input("id"))]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function putCommentAction(Request $request)
    {
        if (!$request->user()) {
            return response()->json(["message" => "Unauthorized"], 401);
        }
        $message = trim(nl2br(strip_tags($request->input("content"))));
        if (!$message) {
            return response()->json(["error" => "U cant's send empty message"], 400);
        }
        Comment::query()->create([
            "user_id" => $request->user()->id,
            "word_id" => $request->input("word_id"),
            "content" => $message
        ]);
        $service = new Service();
        return response()->json(["comments" => $service->getComments($request->input("word_id"))]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCommentAction(Request $request)
    {
        $comment = Comment::query()->find($request->input("comment_id"));
        $wordId = $comment->word_id;
        $comment->delete();
        $service = new Service();
        return response()->json(["comments" => $service->getComments($wordId)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadAlphabet(Request $request)
    {
        $service = new Service();
        return response()->json(["alphabet" => $service->alphabet()]);
    }
}
