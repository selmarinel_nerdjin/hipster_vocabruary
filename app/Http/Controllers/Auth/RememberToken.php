<?php

namespace App\Http\Controllers\Auth;


use App\UserToken;
use Carbon\Carbon;

trait RememberToken
{
    protected function setToken()
    {
        $user = $this->guard()->user();
        if (!$user) {
            return null;
        }
        UserToken::createToken($user);
    }

    protected function forgotToken()
    {
        $user = $this->guard()->user();
        if (!$user) {
            return null;
        }
        $token = UserToken::query()->where("user_id",$user->id)->first();
        if(!$token){
            return null;
        }
        $token->expired_at = (new Carbon())->modify("-5 second");
        $token->save();
    }
}