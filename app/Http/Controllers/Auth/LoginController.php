<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, RememberToken;

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $this->setToken();
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getUserAction(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string'
        ]);
        return response()->json(["user" => User::query()->where("email", $request->input("email"))->first()->getInfo()]);
    }

    /**
     * @deprecated
     * @param Request $request
     * @return mixed
     */
    public function facebookLogAction(Request $request)
    {
        return Socialite::driver('facebook')->scopes([
            'email', 'user_birthday', 'public_profile', 'user_location'
        ])->asPopup()->redirect();
    }

    /** @deprecated */
    public function facebookAuthAction(Request $request)
    {
        if ($request->get("code")) {
            try {
                $driver = Socialite::driver('facebook')->fields([
                    'name', 'email', 'gender', 'birthday', 'location'
                ])->user();
            } catch (ConnectException $connectException) {
                $driver = null;
                app('sentry')->captureException($connectException);
            }
            if ($driver) {
                $user = User::createOrGetUser($driver);
                auth()->login($user);
                if (!$user->remember_token) {
                    $user->remember_token = str_random(10);
                    $user->save();
                }
            }
            return redirect()->to(($request->server("HTTP_REFERER")) ?: route("home"));
        }
        return abort(403);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        return redirect(($request->server("HTTP_REFERER")) ?: route("home"));
    }

    public function fbLoginAction(Request $request)
    {
        $token = $request->input("token");
        $socialUser = Socialite::driver('facebook')->userFromToken($token);
        if ($socialUser) {
            $user = User::createOrGetUser($socialUser);
            auth()->login($user);
            if (!$user->remember_token) {
                $user->remember_token = str_random(10);
                $user->save();
            }
        }
        return response()->json(["redirect" => ($request->server("HTTP_REFERER")) ?: route("page")]);
    }
}
