<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setLocale(Request $request, $locale)
    {
        Session::put('locale', $locale);
        App::setLocale($locale);
        return redirect(app('Illuminate\Routing\UrlGenerator')->previous());

    }
}
