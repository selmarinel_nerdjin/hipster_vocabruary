<?php

namespace App\Http\Controllers\Api;

use App\Bundles\User\Likes\UserLike;
use App\Bundles\Word\Handle;
use App\Http\Controllers\Controller;
use App\Models\Word;
use App\User;
use App\UserToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class WordController extends Controller
{
    /**
     * @var null|User
     */
    private $user = null;
    /** @var  Handle */
    private $service;

    /**
     * WordController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->setUser($request->input("token"));
        $this->service = new Handle($this->getUser());
    }

    /**
     * @param $token
     */
    protected function setUser($token)
    {
        $tokenEntity = UserToken::query()
            ->where("token", "=", $token)
            ->where("expired_at", ">=", (new Carbon())->format("Y-m-d H:i:s"))
            ->first();
        if ($tokenEntity) {
            $this->user = $tokenEntity->user;
        }
    }

    /**
     * @return User|null
     */
    protected function getUser()
    {
        return $this->user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAction(Request $request)
    {
        $validatorRules = [
            'id' => 'required|exists:words,id',
        ];
        $validator = Validator::make($request->all(), $validatorRules);

        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 403);
        }
        $id = $request->get("id");
        /** @var Word $word */
        $word = $this->service->getWordById($id);
        if (!$word) {
            return response()->json(["message" => "Not Found"], 404);
        }
        return response()->json(["word" => $this->prepareWord($word)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNearAction(Request $request)
    {
        $validatorRules = [
            'word' => 'required|exists:words,name',
            'take' => ''
        ];
        $validator = Validator::make($request->all(), $validatorRules);

        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 403);
        }
        $word = $request->get("word");
        $take = ($request->get("take")) ?: 10;
        $this->service->setTake($take);
        $collection = $this->service->findNearWords($word);
        return response()->json(["collection" => $collection]);
    }

    public function prepareWord(Word $word)
    {
        $userLike = new UserLike($this->getUser());
        return array_merge($word->getFullInformation(), ["like" => $userLike->isLiked($word)]);
    }
}