<?php

namespace App\Http\Controllers\Admin;


use App\Events\UpdateWordsEvent;
use App\Http\Controllers\Controller;
use App\Models\FilterToWord;
use App\Models\Roles;
use App\Models\RoleToUser;
use App\Models\Rule;
use App\Models\RuleToRole;
use App\Models\UserInformation;
use App\Models\Word;
use App\Models\WordToUser;
use App\Models\WordToWords;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;
use Validator;

class AdminController extends Controller
{

    public function getPermissions(Request $request)
    {
        if (!$request->user()) {
            return response()->json(["permissions" => []]);
        }
        $permissions = [];
        Rule::query()->get()->map(function ($model) use (&$permissions) {
            $permissions[md5($model->name)] = 1;
        });
        $userPermissions = [];
        $request->user()->getRole()->rules->map(function ($model) use (&$userPermissions) {
            $userPermissions[md5($model->name)] = 1;
        });
        return response()->json(["permissions" => array_intersect($userPermissions,$permissions)]);
    }

    public function activeAction(Request $request)
    {
        if (!$request->user()) {
            return response()->json(["message" => "Unauthorized"], 401);
        }
        if (!$request->user()->isAdmin()) {
            return response()->json(["message" => "You have not permissions"], 401);
        }
        if ($request->input("id")) {
            $model = Word::query()->find($request->input("id"));
            if ($model->status === Word::SUBMITTED) {
                $model->status = Word::ACTIVE;
            } elseif ($model->status === Word::ACTIVE) {
                $model->status = Word::DISABLE;
            } else {
                $model->status = Word::ACTIVE;
            }
            $model->save();
            event(new UpdateWordsEvent());
            return response()->json(["status" => $model->status]);
        }
    }

    public function createAction(Request $request)
    {
        if (!$request->user()) {
            return response()->json(["message" => "Unauthorized"], 401);
        }
        $validatorRules = [
            'name' => 'required|unique:words,name',
            'description' => 'required|min:1'
        ];
        if ($request->input("id")) {
            $validatorRules['name'] = 'required';
        }
        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 403);
        }

        if ($request->input("id")) {
            $model = Word::query()->find($request->input("id"));
        } else {
            $model = new Word();
            $model->user_id = $request->user()->id;
        }
        $model->name = $request->input("name");
        $model->letter = mb_substr($request->input("name"), 0, 1);
        $model->description = $request->input("description");
        $model->save();

        $tags = $request->input("tags");

        $oldTags = WordToWords::query()->where("word_id", $model->id)->get();
        $oldTagsName = $oldTags->map(function ($tag) {
            if ($tag->tag) {
                return $tag->tag->name;
            }
        })->toArray();
        if (is_array($tags) && !empty($tags)) {
            foreach (array_diff($oldTagsName, $tags) as $tag) {
                Word::query()->where("name", $tag)->delete();
            }
            foreach ($tags as $tag) {
                $tagWord = Word::query()->where("name", $tag)->first();
                WordToWords::createRelation($model->id, $tagWord->id);
            }
        }
        $filters = $request->input("filters");
        $filtersToSave = [];
        if ($filters) {
            foreach ($filters as $filter => $value) {
                if ($value == 'true' || $value == "on") {
                    $filtersToSave[] = $filter;
                }
            }
            $oldFilters = FilterToWord::query()->where("word_id", $model->id)->get()->map(function ($relate) {
                return $relate->filter_id;
            })->toArray();;
            if ($oldFilters) {
                foreach (array_diff($oldFilters, $filtersToSave) as $filter) {
                    FilterToWord::query()->where("filter_id", $filter)->delete();
                }
            }
            foreach ($filtersToSave as $filter) {
                FilterToWord::withTrashed()->where("word_id", $model->id)
                    ->where("filter_id", $filter)->restore();
                FilterToWord::query()->updateOrCreate(["word_id" => $model->id, "filter_id" => $filter], ["deleted_at" => null]);
            }
        }

        return response()->json(["word" => $model]);
    }

    public function dashboard(Request $request)
    {
        return view("admin.dashboard");
    }

    public function wordsAction(Request $request)
    {
        return view("admin.words");
    }

    public function usersAction(Request $request)
    {
        return view("admin.users");
    }

    public function userSaveAction(Request $request)
    {
        $userInformation = $request->input("user");
        if ($userInformation && $userInformation['id']) {
            UserInformation::query()
                ->updateOrCreate(["user_id" => $userInformation['id']], [
                    'age' => $userInformation['details']['age'],
                    'gender' => UserInformation::processGender($userInformation['details']['gender'])
                ]);
            RoleToUser::query()
                ->updateOrCreate(["user_id" => $userInformation['id']], ["role_id" => $userInformation['role_id']]);

        }
    }

    public function getRolesAction(Request $request)
    {
        $collection = Roles::query()->get();
        return response()->json(["roles" => $collection]);

    }

}