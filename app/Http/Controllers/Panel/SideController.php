<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Filter;
use App\Models\Roles;
use App\Models\RoleToUser;
use App\Models\UserInformation;
use App\Services\FilterService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Validator;

class SideController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new UserService();
    }

    public function getUsers(Request $request)
    {
        return response()->json(["users" => $this->service->getUsers()]);
    }

    public function getUsersAPI(Request $request)
    {
        $offset = (int)$request->input("limit") * ((int)$request->input("page") - 1);
        $collection = $this->service->getUsers($request->input("query"), $request->input("limit"), $offset);
        return response()->json([
            "users" => $collection,
            "page" => (int)$request->input("page"),
            "count" => $this->service->getUsers($request->input("query"))->count()
        ]);
    }

    public function getUser(Request $request)
    {
        return response()->json(["user" => $this->service->getUser($request->input("id"))]);
    }

    public function updateUser(Request $request)
    {
        $data = $request->all();
        if (!$request->user()) {
            return response()->json(["fail"], 400);
        }
        if ($request->user()->id == $data['id'] || $request->user()->isSuperUser()) {
            $user = User::query()->updateOrCreate([
                'id' => $data['id'],
            ], [
                'name' => $data['name'],
            ]);
            if (isset($data['password']) && $data["password"]) {
                $user->password = bcrypt($data['password']);
                $user->save();
            }

            UserInformation::query()->updateOrCreate([
                'user_id' => $user->id], [
                'age' => $data['age'],
                "gender" => UserInformation::processGender($data['gender']),
                "locale" => $data['location'],
            ]);
            return $user;
        }
        return response()->json(["fail"], 400);
    }

    private function changeUserRole(Request $request, $role)
    {
        $validatorRules = [
            'user_id' => 'required|exists:users,id',
        ];
        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 404);
        }
        if (RoleToUser::query()
            ->where("user_id", $request->input("user_id"))
            ->where("role_id", Roles::SUPERUSER)
            ->exists()
        ) {
            return response()->json(["message" => "You don't have permissions"], 400);
        }
        RoleToUser::query()->updateOrCreate(["user_id" => $request->input("user_id")], ["role_id" => $role]);
        return response()->json(["user" => $this->service->getUser($request->input("user_id"))]);
    }

    public function confirmUser(Request $request)
    {
        return $this->changeUserRole($request, ($request->input("role_id")) ? $request->input("role_id") : Roles::VERIFIED_USER);
    }

    public function verifyUser(Request $request)
    {
        return $this->changeUserRole($request, Roles::VERIFIED_USER);
    }

    public function banUser(Request $request)
    {
        return $this->changeUserRole($request, Roles::USER);
    }

    public function superUser(Request $request)
    {
        return $this->changeUserRole($request, Roles::SUPERUSER);
    }

    public function getFilter(Request $request)
    {
        return response()->json(['list' => FilterService::getList()]);
    }

    public function getFilters(Request $request)
    {
        return response()->json(['list' => FilterService::getFullList()]);
    }

    public function putFilter(Request $request)
    {
        $validatorRules = [
            "id" => "required|exists:filters,id",
            "name" => "required"
        ];
        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 404);
        }
        Filter::query()->where("id", $request->input("id"))->update(["name" => $request->input("name")]);
        return response()->json(["success"]);
    }

    public function deleteFilter(Request $request)
    {
        $validatorRules = [
            "id" => "required|exists:filters,id",
        ];
        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 404);
        }
        Filter::query()->where("id", $request->input("id"))->delete();
        return response()->json(["success"]);
    }

    public function createFilter(Request $request)
    {
        $validatorRules = [
            "name" => "required",
        ];
        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 404);
        }
        Filter::query()->create(["name" => $request->input("name")]);
        return response()->json(["success"]);
    }

    public function moderatorUser(Request $request)
    {
        return $this->changeUserRole($request, Roles::MODERATOR);
    }

    public function reporterUser(Request $request)
    {
        return $this->changeUserRole($request, Roles::REPORTER);
    }

    public function verifiedUser(Request $request)
    {
        return $this->changeUserRole($request, Roles::VERIFIED_USER);
    }

    public function securityAuth(Request $request)
    {
        if ($request->input("user_id")) {
            /** @var User $user */
            $user = $this->service->findUserModel($request->input("user_id"));
            if ($user) {
                auth()->login($user);
            }
            return response()->json(["redirect" => route("home")]);
        }
        return response()->json(["error" => "Invalid user"], 404);
    }
}