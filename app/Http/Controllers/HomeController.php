<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Session;
use App\Models\Word;
use App\Services\FilterService;
use App\Services\Word\Service;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Auth\RememberToken;

class HomeController extends Controller
{
    use AuthenticatesUsers, RememberToken;

    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function landing(Request $request)
    {
        return view('home');
    }

    public function get(Request $request)
    {
        $handle = new App\Services\WordBundle\WordHandle($request->user());
        return response()->json(["word" => $handle->getOneWord($request->input("word"))]);
    }

    public function find(Request $request)
    {
        $validatorRules = [
            'query' => 'required|min:3'
        ];

        $validator = Validator::make($request->all(), $validatorRules);
        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 404);
        }

        $handle = new App\Services\WordBundle\WordHandle($request->user());
        return response()->json(['words' => $handle->find($request->input("query"))]);
    }

    public function page(Request $request)
    {
        return view("page", [
            "count" => 0,
            "word" => "",
            "filters" => [],
            "wordModel" => new Word(),
        ]);
    }

    public function index(Request $request, $word = null)
    {
        try {
            if ($request->hasSession()) {
                if ($request->session()->get('locale')) {
                    App::setLocale($request->session()->get("locale"));
                }
            }
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }
        $service = new Service($word, $request->user());
        return view("page", [
            "count" => Word::query()->active()->count(),
            "word" => $word,
            "filters" => FilterService::getList(),
            "wordModel" => ($service->hasErrors()) ? $service->getEmptyModel() : $service->getWordModel()
        ]);
    }

    public function loginAction(Request $request)
    {
        $loginResult = $this->login($request);
        return response()->json($loginResult);
    }

    public function logoutAction(Request $request)
    {
        $this->forgotToken();
        $this->guard()->logout();
        $request->session()->invalidate();
        return response()->json(["redirect" => route("home")]);
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        $this->setToken();
        if ($request->input("word")) {
            return ["redirect" => route("home", ["word" => $request->input("word")])];
        }
        return ["redirect" => route("home")];
    }
}
