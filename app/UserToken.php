<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = "user_tokens";

    protected $fillable = [
        "user_id",
        "token",
        "expired_at"
    ];

    public function user()
    {
        return $this->hasOne(User::class,"id","user_id");
    }

    public static function createToken(User $user)
    {
        self::query()->updateOrCreate(["user_id" => $user->id], [
            "expired_at" => (new Carbon())->modify("+1 hour"),
            "token" => static::setToken()
        ]);
    }

    protected static function setToken()
    {
        $token = str_random(100);
        if (self::query()->where("token", $token)->exists()) {
            self::setToken();
        }
        return $token;
    }

}
