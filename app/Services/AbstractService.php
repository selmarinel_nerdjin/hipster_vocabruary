<?php

namespace App\Services;


class AbstractService
{
    protected $errors = [];

    protected function addError($error)
    {
        $this->errors[] = $error;
    }

    protected function eraseErrors()
    {
        $this->errors = [];
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function setNotFoundModelError($modelName = null)
    {
        $this->addError((($modelName) ?: class_basename($this)) . " not found");
    }
}