<?php

namespace App\Services\Word;

use App\Models\Comment;
use App\Models\Word;
use App\Models\WordToUser;
use App\Services\AbstractService;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use DB;

class Service extends AbstractService
{
    /** @var string */
    private $word;
    /**
     * @var Word|null
     */
    private $model;

    private $letter;

    private $administrateRule;

    private $relations = ["tags", "likes", "comments", "filters"];
    /** @var  User */
    private $user;

    public function __construct($word = null, User $user = null)
    {
        $this->setUser($user);
        $this->setWord($word);
        $this->setWordModel();

    }

    public function setUser(User $user = null)
    {
        $this->user = $user;
    }

    public function setWord($word)
    {
        $this->word = $word;
        $this->setWordModel();
    }

    public function setWordModel()
    {
        $this->eraseErrors();
        if (!$this->word) {
            $this->setNotFoundModelError("Word");
        }
        if (!$this->hasErrors()) {
            $builder = Word::with('tags', 'likes')
                ->where('name', $this->word);
            $this->checkOwner($builder);
            $this->model = $builder->first();
        }
        if (!$this->model) {
            $this->setNotFoundModelError("Word");
        }
    }

    public function justGiveList()
    {
        $this->eraseErrors();
        $builder = Word::with($this->relations)->byLetter($this->letter);
        $this->checkOwner($builder);
        return $builder
            ->inRandomOrder()
            ->take(self::LIMIT * 2 + 1)
            ->get();
    }

    public function getWordModel($word = null)
    {
        $this->eraseErrors();
        if ($word) {
            $this->setWord($word);
        }
        if (!$this->model) {
            $this->setNotFoundModelError("Word");
        }
        return $this->model;
    }

    public function getRandomWordModel()
    {
        $this->eraseErrors();
        $builder = Word::with($this->relations)
            ->byLetter($this->letter);
        $this->checkOwner($builder);
        $this->model = $builder->inRandomOrder()->first();
        if (!$this->model) {
            return [];
        }
        $this->word = $this->model->name;
        return $this->getWordModel();
    }

    public function findByWord($search)
    {
        $this->eraseErrors();
        $queryBuilder = Word::with($this->relations)
            ->where('name', 'like', "{$search}%")
            ->byLetter($this->letter);
        $this->checkOwner($queryBuilder);

        $words = $queryBuilder->get();
        if (!$words->first()) {
            $this->setNotFoundModelError("Words");
        }
        $this->model = $words->first();
        return $words;
    }

    public function setLetter($letter = null)
    {
        $this->letter = $letter;
    }


    public function setAdministrateRules($bool)
    {
        $this->administrateRule = $bool;
    }

    const LIMIT = 5;

    private function checkOwner($query)
    {
        $user = $this->user;
        if ($user && !$this->administrateRule) {
            $admin = $this->administrateRule;
            $query->where(function ($subQuery) use ($admin, $user) {
                $subQuery->active($admin)->orOwner($user);
            });
        } else {
            $query->active($this->administrateRule);
        }
    }

    public function getNearWordsByWord()
    {
        $this->eraseErrors();
        if ($this->word) {
            $queryBuilderLess = Word::with($this->relations)
                ->nearLess($this->word)
                ->byLetter($this->letter);
            $this->checkOwner($queryBuilderLess);
            $queryBuilderMore = Word::with($this->relations)
                ->nearMore($this->word)
                ->byLetter($this->letter);
            $this->checkOwner($queryBuilderMore);
            $modelBuilder = Word::with($this->relations)
                ->where("name", $this->word);
            $this->checkOwner($modelBuilder);
            return $modelBuilder
                ->byLetter($this->letter)
                ->union($queryBuilderLess->orderByDesc("name")->take(5))
                ->union($queryBuilderMore->take(5))
                ->take(self::LIMIT * 2 + 1)
                ->get();
        }
        $this->setNotFoundModelError("Word");
        return [];
    }

    public function alphabet()
    {

        $collection = Word::query()
            ->active($this->administrateRule)
            ->select("letter")
            ->groupBy("letter")
            ->orderByDesc("letter")
            ->get()
            ->map(function ($model) {
                return mb_strtolower(mb_substr($model->letter, 0, 1));
            })->unique();
        $result = [];
        foreach ($collection as $model) {
            $result[] = $model;
        }
        $currentLocale = setlocale(LC_ALL, NULL);
        setlocale(LC_ALL, 'uk_UA.utf8');
        sort($result, SORT_LOCALE_STRING);
        setlocale(LC_ALL, $currentLocale);
        return array_map(function ($letter) {
            return mb_strtoupper($letter);
        }, $result);
    }

    public function getComments($word_id = null)
    {
        return Comment::with('user', 'user.information', 'user.information.cover', 'likes')
            ->where("word_id", ($word_id) ?: $this->model->id)
            ->orderByDesc('created_at')
            ->get()
            ->map(function ($comment) {
                /** @var Comment $comment */
                return $comment->getInfo();
            });
    }

    /**
     * @return Word
     */
    public function getEmptyModel()
    {
        $model = new Word();
        $model->name = "Not Found";
        $model->description = "Sorry, but this word doesn't exists";
        $model->created_at = (new \DateTime())->format("Y-m-d H:i:s");
        return $model;
    }

    public function getPopularList()
    {
        $queryBuilder = Word::with($this->relations);
        $this->checkOwner($queryBuilder);
        $queryBuilder->select(DB::raw("
        *,
        (SELECT SUM(`word_to_user`.like)
            FROM `word_to_user`
            WHERE `word_to_user`.word_id = `words`.id
            GROUP BY `word_to_user`.word_id
        ) as likesCount"));
        $queryBuilder->byLetter($this->letter);
        $queryBuilder->orderByDesc("likesCount");
        return $queryBuilder->take(10)->get();
    }

    public function getNewestList()
    {
        $queryBuilder = Word::with($this->relations);
        $this->checkOwner($queryBuilder);
        $queryBuilder->byLetter($this->letter);
        $queryBuilder->orderByDesc("created_at");
        if ($this->user && $this->user->getRole()->canVerify()) {
            $queryBuilder->whereIn("status",
                [
                    Word::DISABLE,
                    Word::DELETED,
                    Word::SUBMITTED
                ]);
        }
        return $queryBuilder->take(10)->get();
    }
}