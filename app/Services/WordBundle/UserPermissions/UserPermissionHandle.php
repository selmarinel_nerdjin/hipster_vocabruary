<?php
/**
 * Created by PhpStorm.
 * User: selma
 * Date: 02.11.2017
 * Time: 20:06
 */

namespace App\Services\WordBundle\UserPermissions;


use App\Services\PermissionService;
use App\User;

class UserPermissionHandle
{
    /** @var User */
    private $user;

    private $permissions;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->permissions = new PermissionService();
    }

    public function isAllow($rule)
    {
        return $this->user->isSuperUser() || $this->permissions->can($this->user, $rule);
    }
}