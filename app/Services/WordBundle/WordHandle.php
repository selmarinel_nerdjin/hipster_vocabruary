<?php

namespace App\Services\WordBundle;


use App\Models\Rule;
use App\Models\Word;
use App\Models\WordToWords;
use App\Services\WordBundle\UserPermissions\UserPermissionHandle;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class WordHandle
{
    /** @var  User|null */
    private $user = null;
    /** @var  Word */
    private $word;

    /** @var  Collection */
    private $collectionOfWords;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function getRandomWord($short = false)
    {
        $this->word = $this->prepareBuilderForSingle()->inRandomOrder()->first();
        return $this->prepareWordResult($short);
    }

    public function getOneWord($word, $short = false)
    {
        /** @var Word word */
        $this->word = $this->prepareBuilderForSingle($word)->first();
        return $this->prepareWordResult($short);
    }


    public function prepareWordResult($short)
    {
        if (!$this->word) {
            return [];
        }
        if ($short) {
            return $this->word->getShortInformation();
        }
        return $this->word->getFullInformation();
    }

    public function getWord()
    {
        return $this->word;
    }

    public function getWordList()
    {
        return $this->collectionOfWords;
    }

    /**
     * @param $word
     * @return Builder
     */
    public function prepareBuilderForSingle($word = null)
    {
        $builder = Word::with('tags', 'likes');
        if ($word) {
            $builder->where('name', $word);
        }
        $user = $this->user;
        if ($user && $user instanceof User) {
            $permissions = new UserPermissionHandle($user);
            $allow = $permissions->isAllow(Rule::CAN_EDIT_WORD);
            return $builder->where(function ($subQuery) use ($allow, $user) {
                $subQuery->active($allow);
                if (!$allow) {
                    $subQuery->orOwner($user);
                }
            });
        }
        return $builder->active();
    }

    /** @var array */
    private $relations = ["tags", "likes", "comments", "filters"];

    public function getListOfWords($word, $letter = null)
    {
        if ($word) {
            $queryBuilderLess = Word::with($this->relations)
                ->nearLess($word)
                ->byLetter($letter);
            $this->checkOwner($queryBuilderLess);
            $queryBuilderMore = Word::with($this->relations)
                ->nearMore($word)
                ->byLetter($letter);
            $this->checkOwner($queryBuilderMore);
            $modelBuilder = Word::with($this->relations)
                ->where("name", $word);
            $this->checkOwner($modelBuilder);
            return $modelBuilder
                ->byLetter($letter)
                ->union($queryBuilderLess->orderByDesc("name")->take(5))
                ->union($queryBuilderMore->take(5))
                ->take(5 * 2 + 1)
                ->get();
        }
        return [];
    }

    public function getList()
    {

    }

    public function find($word)
    {
        $user = $this->user;
        $allow = false;
        if ($user && $user instanceof User) {
            $permissions = new UserPermissionHandle($user);
            $allow = $permissions->isAllow(Rule::CAN_EDIT_WORD);
        }
        /** find by word name */
        $words = Word::with($this->relations)
            ->where("name", "like", "%$word%")
            ->active($allow)
            ->get();

        $result = new Collection($words);
        foreach ($words as $word) {
            foreach ($word->tags as $tag) {
                $result->add($tag);
            }
        }
        return $result->unique()->map(function ($model) {
            /** @var Word $model */
            return $model->getShortInformation();
        });
    }

}