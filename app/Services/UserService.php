<?php

namespace App\Services;


use App\User;

class UserService
{
    /** @var  User */
    private $userModel;

    /**
     * @param User $user
     */
    public function setUserModel($user)
    {
        $this->userModel = $user;
    }


    /**
     * @return User
     */
    public function getUserModel(): User
    {
        return $this->userModel;
    }

    /**
     * @param $id
     * @return User
     */
    public function findUserModel($id): User
    {
        $model = User::query()->find($id);
        if (!$model) {
            $model = User::getDefault();
        }
        $this->setUserModel($model);
        return $this->getUserModel();
    }

    public function getUser($id)
    {
        $model = $this->findUserModel($id);
        return $this->getUserInformation($model);
    }

    public function getUserInformation(User $model)
    {
        return [
            "id" => $model->id,
            "name" => $model->name,
            "email" => $model->email,
            "created" => (new \DateTime($model->created_at))->format("Y-m-d H:i:s"),
            "details" => $model->details,
            "cover" => ($model->details) ? $model->details->getCover() : "img/user.png",
            "role" => $model->getRole()->name,
            "role_id" => $model->getRole()->id,
            "token" => md5($model->id . $model->getRole()->id)
        ];
    }

    public function getUsers($query = null, $limit = null, $offset = null)
    {
        $query = User::with("information")
            ->where("name", "like", "%{$query}%")
            ->orderBy("created_at", "desc");
        if ($limit) {
            $query = $query->take($limit);
        }
        if ($offset) {
            $query = $query->offset($offset);
        }
        $collection = $query->get();
        return $collection->map(function ($user) {
            /** @var User $user */
            return $this->getUserInformation($user);
        });
    }
}