<?php

namespace App\Services;


use App\Models\Filter;

class FilterService extends AbstractService
{
    public static function getList()
    {
        $collection = Filter::query()->get();
        $result = [];
        foreach ($collection as $model) {
            $result[$model->id] = $model->name;
        }
        return $result;
    }

    public static function getFullList()
    {
        return Filter::query()->get();
    }

}