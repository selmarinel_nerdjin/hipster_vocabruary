<?php

namespace App\Services;

use App\Models\RuleToRole;
use App\User;

class PermissionService
{
    public function can(User $who, $what)
    {
        $role = $who->getRole();
        return RuleToRole::query()
            ->where("role_id", $role->id)
            ->where("rule_id", $what)
            ->exists();
    }
}