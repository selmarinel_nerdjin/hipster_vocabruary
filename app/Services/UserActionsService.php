<?php
/**
 * Created by PhpStorm.
 * User: selmarinel
 * Date: 24.07.17
 * Time: 14:59
 */

namespace App\Services;


use App\Models\Word;
use App\Models\WordToUser;
use App\User;

class UserActionsService
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function gusto($word, $like = true)
    {
        /** @var Word $word */
        $word = Word::query()->where("name", $word)->first();
        if ($word && $this->user) {
            WordToUser::query()->updateOrCreate(["word_id" => $word->id, "user_id" => $this->user->id], ["like" => $like])->lockForUpdate();
            $people = WordToUser::query()
                ->where("word_id", $word->id)
                ->where("like", true)
                ->get()
                ->map(function ($model) {
                return $model->user;
            });
            return [
                "likes" => $word->likes,
                "people" => $people,
                "like" => $like
            ];
        }
        return false;
    }

    public function like($wordId)
    {
        if ($this->user) {
            WordToUser::query()->updateOrCreate(["word_id" => $wordId, "user_id" => $this->user->id], ["like" => true])->lockForUpdate();
            return true;
        }
        return false;
    }

    public function dislike($wordId)
    {
        if ($this->user) {
            WordToUser::query()->updateOrCreate(["word_id" => $wordId, "user_id" => $this->user->id], ["like" => false])->lockForUpdate();
            return true;
        }
        return false;
    }

    public function getLiked($wordId)
    {
        if ($this->user) {
            $model = WordToUser::query()->where("word_id", $wordId)->where("user_id", $this->user->id)->first();
            if ($model) {
                return $model->like;
            }
        }
        return false;
    }

    public function getLikedInformation(Word $word)
    {
        $query = WordToUser::query()->where("word_id", $word->id);
        $people = $query->where("like", true)->get()->map(function ($model) {
            return $model->user;
        });
        if ($this->user) {
            $model = $query->where("user_id", $this->user->id)->first();
        }
        return [
            "author" => $word->getAuthor(),
            "like" => (isset($model) && $model) ? $model->like : false,
            "people" => array_slice($people->toArray(), 0, 3)
        ];
    }


}