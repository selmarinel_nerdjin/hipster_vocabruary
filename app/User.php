<?php

namespace App;

use App\Models\File;
use App\Models\Roles;
use App\Models\RoleToUser;
use App\Models\SocialAccounts;
use App\Models\UserInformation;
use Faker\Provider\Lorem;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Socialite\Contracts\User as ProviderUser;

class User extends Authenticatable
{
    use Notifiable;

    protected $with = [
        "role"
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at'
    ];

    public static function getDefault()
    {
        $model = new self();
        $model->name = "System";
        $model->email = "system@i.io";
        return $model;
    }

    public function role()
    {
        return $this->hasOne(RoleToUser::class, "user_id", "id");
    }

    public function isAdmin()
    {
        return in_array($this->role->role->id, [Roles::ADMIN, Roles::SUPERUSER]);
    }

    public function isSuperUser()
    {
        return $this->role->role->id == Roles::SUPERUSER;
    }

    public function getInfo()
    {
        return ["email" => $this->attributes["email"]];
    }

    private static function setInformation(User $user, ProviderUser $providerUser)
    {
        try {
            $cover = File::query()->firstOrCreate(["path" => $providerUser->getAvatar()]);
            UserInformation::query()->updateOrCreate(["user_id" => $user->id], [
                "age" => ($providerUser->user['birthday']) ? (new \DateTime($providerUser->user['birthday']))->diff(new \DateTime())->y : 13,
                "gender" => ($providerUser->user) ? UserInformation::processGender($providerUser->user['gender']) : UserInformation::GENDER_ALIEN,
                "birth" => ($providerUser->user['birthday']) ? (new \DateTime($providerUser->user['birthday']))->format("Y-m-d") : (new \DateTime())->format("Y-m-d H:i:s"),
                "locale" => ($providerUser->user && $providerUser->user["location"]) ? $providerUser->user['location']['name'] : "",
                "cover_id" => $cover->id
            ]);
        } catch (\Exception $exception) {
            /** TODO */
        }
    }

    /**
     * @param ProviderUser $providerUser
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public static function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccounts::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            self::setInformation($account->user, $providerUser);
            return $account->user;
        }
        $account = new SocialAccounts([
            'provider_user_id' => $providerUser->getId(),
            'provider' => 'facebook'
        ]);
        $user = self::whereEmail($providerUser->getEmail())->first();
        if (!$user) {
            $user = self::create([
                'email' => $providerUser->getEmail(),
                'name' => $providerUser->getName(),
                'password' => bcrypt(Lorem::word()),
                'remember_token' => str_random(10),
            ]);
        }
        RoleToUser::query()->firstOrCreate(["user_id" => $user->id], ["role_id" => Roles::VERIFIED_USER]);
        $account->user()->associate($user);
        $account->save();
        self::setInformation($user, $providerUser);
        return $user;
    }

    public function information()
    {
        return $this->hasOne(UserInformation::class, "user_id", "id");
    }

    public function getDetailsAttribute()
    {
        return ($this->information) ? $this->information : UserInformation::getDefault();
    }

    public function getCover()
    {
        $information = $this->getDetailsAttribute();
        return $information->getCover();
    }

    /**
     * @return Roles
     */
    public function getRole()
    {
        if (!$this->role) {
            return Roles::getDefault();
        }
        return $this->role->role;
    }
}
