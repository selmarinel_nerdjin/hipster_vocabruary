<?php

namespace App\Listeners;

use App\Events\UpdateWordsEvent;
use App\Models\Word;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateWordsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateWordsEvent  $event
     * @return void
     */
    public function handle(UpdateWordsEvent $event)
    {
        $options = array(
            'cluster' => 'eu',
            'encrypted' => true
        );
        $pusher = new \Pusher\Pusher(
            'e941a23887c0003b4dc8',
            '329925e2f092997456bc',
            '373005',
            $options
        );
        $data['message'] = 'hello world';
        $pusher->trigger('my-channel', 'words_count', Word::query()->active()->count());
    }
}
