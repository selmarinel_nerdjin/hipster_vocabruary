<?php

namespace App\Console\Commands;

use App\Events\UpdateWordsEvent;
use App\Models\Word;
use Faker\Provider\Lorem;
use Illuminate\Console\Command;
use Mockery\Exception;

class words_generate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'words_generate {mode} {--count=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = ($this->option('count')) ? $this->option('count') : 50;
        $itemList = [];
        if (in_array($this->argument("mode"), ["random", "r"])) {
            for ($i = 0; $i <= $count; $i++) {
                $itemList[] = str_random(rand(5, 20));
            }
        } else {
            $itemList = Lorem::words($count, false);
        }
        foreach ($itemList as $word) {
            if (!Word::query()->where("name", $word)->exists()) {
                factory(Word::class, 1)->create([
                    "name" => $word,
                    "letter" => mb_strtoupper(mb_substr($word, 0, 1)),
                    "description" => Lorem::words(rand(25, 70), true)
                ]);
                $this->info("Success. word: {{$word}}");
            }
        }
        event(new UpdateWordsEvent());
    }

}
