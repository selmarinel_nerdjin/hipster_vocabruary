<?php

namespace App\Console\Commands;

use App\Models\Comment;
use App\Models\Word;
use App\Models\WordToWords;
use App\User;
use Faker\Provider\Lorem;
use Illuminate\Console\Command;

class random_some_words extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'words_random {--count=} {--set=} {--relations=} {--comments=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = ($this->option('count')) ? $this->option('count') : random_int(5, 10);
        $set = ($this->option('set') || $this->option('set') === 0) ? $this->option('set') : false;
        $this->info("Try get $count words");
        $collection = Word::query()->offset(random_int(0, Word::query()->count() - $count))->take($count)->get();
        if ($this->option('relations') == 1) {
            $this->info("work with only relations");
        }
        if ($this->option('comments') == 1) {
            $this->info("work with only comments");
        }
        foreach ($collection as $model) {
            $model->update(["status" => (($set || $set === 0) ?: random_int(0, 2))]);
            $this->info("Success. word: {{$model->name}}. Set to {{$model->status}}");
            if ($this->option('comments') == 1) {
                $countComments = random_int(1, 5);
                for ($i = 0; $i <= $countComments; $i++) {
                    $comment = Lorem::text(random_int(50, 500));
                    Comment::query()->create([
                        "user_id" => User::query()->inRandomOrder()->first()->id,
                        "word_id" => $model->id,
                        "content" => $comment
                    ]);
                }
                $this->info("Success. $i Comments");
            }
            if ($this->option('relations') == 1) {
                $relationCount = random_int(1, 4);
                for ($i = 1; $i <= $relationCount; $i++) {
                    $tag = Word::query()->where("id", "!=", $model->id)->inRandomOrder()->first()->id;
                    WordToWords::createRelation($model->id,$tag);
                }
                $this->info("Add to {$model->name} {$relationCount} relates");
            }
        }
        $this->info("Success. The End");
    }
}
