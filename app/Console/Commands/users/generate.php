<?php

namespace App\Console\Commands\users;

use App\Models\Roles;
use App\Models\RoleToUser;
use App\User;
use Faker\Provider\Lorem;
use Illuminate\Console\Command;

class generate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 0; $i <= 10; $i++) {
            $name = Lorem::word();
            $user = User::query()->updateOrCreate([
                "email" => "$name@mail.com",
            ], [
                "name" => $name,
                "password" => crypt(Lorem::word(), time())
            ]);
            RoleToUser::query()->updateOrCreate(["user_id" => $user->id], ["role_id" => Roles::VERIFIED_USER]);
            $this->info("Generate user $name");
        }
    }
}
