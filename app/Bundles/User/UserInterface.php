<?php

namespace App\Bundles\User;


interface UserInterface
{
    public function getUserById($id);
}