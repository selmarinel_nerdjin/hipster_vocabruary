<?php

namespace App\Bundles\User\Likes;

use App\Models\Word;

interface LikeInterface
{
    public function isLiked(Word $word);
}