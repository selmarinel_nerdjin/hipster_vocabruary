<?php

namespace App\Bundles\User\Likes;


use App\Models\Word;
use App\Models\WordToUser;
use App\User;

class UserLike implements LikeInterface
{
    private $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * @param Word $word
     * @return bool
     */
    public function isLiked(Word $word)
    {
        if (!$this->user) {
            return false;
        }
        $relate = WordToUser::query()
            ->where("user_id", $this->user->id)
            ->where("word_id", $word->id)
            ->first();
        if (!$relate) {
            return false;
        }
        return (bool)$relate->like;
    }
}