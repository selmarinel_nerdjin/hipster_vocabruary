<?php

namespace App\Bundles\User\Permissions;


interface PermissionInterface
{
    public function isAllow();
}