<?php

namespace App\Bundles\User\Permissions;


use App\User;

class CanSeeWord implements PermissionInterface
{
    /** @var User */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function isAllow()
    {
       return $this->user->getRole()->canEditWord();
    }
}