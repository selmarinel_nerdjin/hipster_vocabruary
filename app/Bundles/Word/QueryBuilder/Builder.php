<?php

namespace App\Bundles\Word\QueryBuilder;

use App\Bundles\User\Permissions\CanSeeWord;
use App\Models\Word;
use App\User;

class Builder
{
    private $relations = [
        "comments",
        "likes",
        "author",
        "filters",
        "author"
    ];
    /**
     * @var null
     */
    private $letterLimitation = null;

    /**
     * @param $letter
     */
    public function setLetter($letter)
    {
        $this->letterLimitation = $letter;
    }

    /**
     * @return string|null
     */
    public function getLetter()
    {
        return $this->letterLimitation;
    }

    /**
     * @return array
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @param array $relations
     */
    public function setRelations($relations)
    {
        $this->relations = $relations;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /** @var \Illuminate\Database\Eloquent\Builder */
    private $builder;

    public function prepareQuery()
    {
        /** @var Builder $builder */
        $this->builder = (new Word())->newQuery()->with($this->getRelations());
    }

    /**
     * @param User|null $user
     * @return null
     */
    public function checkOwner(User $user = null)
    {
        if (!$user) {
            $this->builder->active();
            return null;
        }
        $permissionHandle = new CanSeeWord($user);
        if (!$permissionHandle->isAllow()) {
            $this->builder->active();
        }
        return null;
    }

    public function checkLetter()
    {
        if ($this->getLetter()) {
            $this->builder->byLetter($this->getLetter());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function findId($id)
    {
        if (!$this->builder) {
            $this->prepareQuery();
        }
        return $this->builder
            ->where("id", $id);
    }

    /**
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function findName($name)
    {
        if (!$this->builder) {
            $this->prepareQuery();
        }
        return $this->builder
            ->where("name", $name);
    }

    /**
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function likeName($name)
    {
        if (!$this->builder) {
            $this->prepareQuery();
        }
        $this->checkLetter();
        return $this->builder
            ->where("name", "like", "%$name%");
    }

    /**
     * @param string $word
     * @param int $take
     * @param User|null $user
     */
    public function nearWord(string $word, $take = 10, User $user = null)
    {
        if (!$this->builder) {
            $this->prepareQuery();
        }
        $this->checkLetter();
        $this->builder->nearLess($word);
        $this->checkOwner($user);
        $queryLess = $this->builder;
        $this->prepareQuery();
        $this->checkLetter();
        $this->builder->nearMore($word);
        $this->checkOwner($user);

        $queryMore = $this->builder;
        $this->prepareQuery();
        $this->checkLetter();
        $this->checkOwner($user);

        $this->prepareQuery();
        $this->findName($word);
        $this->builder
            ->union($queryLess->orderByDesc("name")->take(ceil($take / 2)))
            ->union($queryMore->take(ceil($take / 2)));

    }


    /**
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getCollection($limit = 10)
    {
        $this->checkLetter();
        return $this->builder->take($limit)->get();
    }

    /**
     * @param int $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginate($paginate = 10)
    {
        $this->checkLetter();
        return $this->builder->paginate($paginate);
    }

    /**
     * @return Word|null
     */
    public function getEntity()
    {
        return $this->builder->get()->first();
    }
}