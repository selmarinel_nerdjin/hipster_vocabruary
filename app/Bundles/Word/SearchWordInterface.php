<?php

namespace App\Bundles\Word;


interface SearchWordInterface
{
    public function findWordsLikeName($name);

    public function findNearWords($word);

    public function findWordsByLetter($letter);
}