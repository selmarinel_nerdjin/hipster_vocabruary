<?php

namespace App\Bundles\Word\Comment;


use App\Bundles\User\UserInterface;
use App\Bundles\Word\WordInterface;

interface WordCommentInterface
{
    public function getCommentsToWord(WordInterface $word);

    public function addCommentToWord(WordInterface $word, UserInterface $user, $comment);

    public function removeCommentOfWord($commentId);

    public function editCommentOfWord($commentId, $comment);
}