<?php

namespace App\Bundles\Word;

use App\User;
use App\Bundles\Word\QueryBuilder\Builder;

class Handle implements WordInterface, SearchWordInterface, WordConstructorInterface
{
    /**
     * Need to be user to check permissions and abilities to get word
     * While instance created, dev provide user instance to this
     * and process them
     */
    /**
     * Handle constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
        $this->builder = new Builder();
    }

    /**
     * @var Builder
     */
    private $builder;

    /** @var  int */
    private $take = 2;

    /**
     * @var User|null
     */
    private $user = null;

    /**
     * @param $take
     */
    public function setTake($take)
    {
        $this->take = $take;
    }

    /**
     * @return int
     */
    public function getTake()
    {
        return $this->take;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getWordById($id)
    {
        $this->builder->findId($id);
        $this->builder->checkOwner($this->getUser());
        return $this->builder->getEntity();
    }

    /**
     * @param $name
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getWordByName($name)
    {
        $this->builder->findName($name);
        $this->builder->checkOwner($this->getUser());
        return $this->builder->getEntity();
    }

    /**
     * @param $word
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function findNearWords($word)
    {
        $this->builder->nearWord($word, $this->getTake(), $this->getUser());
        return $this->builder->getCollection($this->getTake() + 1);
    }

    /**
     * @param $name
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function findWordsLikeName($name)
    {
        $this->builder->likeName($name);
        $this->builder->checkOwner($this->getUser());
        return $this->builder->getPaginate($this->getTake());
    }

    /**
     * @param $letter
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function findWordsByLetter($letter)
    {
        $this->builder->setLetter($letter);
        $this->builder->prepareQuery();
        return $this->builder->getPaginate();
    }

    public function createWord(array $parameters)
    {
        // TODO: Implement createWord() method.
    }

    public function editWord($id, array $parameters)
    {
        // TODO: Implement editWord() method.
    }

    public function deleteWord($id)
    {
        // TODO: Implement deleteWord() method.
    }

    public function restoreWord($id)
    {
        // TODO: Implement restoreWord() method.
    }

}