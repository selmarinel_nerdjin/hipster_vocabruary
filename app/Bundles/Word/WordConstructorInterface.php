<?php

namespace App\Bundles\Word;


interface WordConstructorInterface
{
    public function createWord(array $parameters);

    public function editWord($id, array $parameters);

    public function deleteWord($id);

    public function restoreWord($id);
}