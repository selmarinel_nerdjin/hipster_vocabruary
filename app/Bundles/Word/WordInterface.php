<?php

namespace App\Bundles\Word;


interface WordInterface
{
    public function getWordById($id);

    public function getWordByName($name);
}