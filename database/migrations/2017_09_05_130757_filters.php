<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Filters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->unique();
            $table->string("value")->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('word_to_filter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("word_id")->references("id")->on("words");
            $table->integer("filter_id")->references("id")->on("filters");
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['word_id', 'filter_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
