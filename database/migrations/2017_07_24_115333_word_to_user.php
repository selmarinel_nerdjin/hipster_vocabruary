<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WordToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_to_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("word_id")->references("id")->on("words");
            $table->integer("user_id")->references("id")->on("user");
            $table->boolean("like");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("word_to_user");
    }
}
