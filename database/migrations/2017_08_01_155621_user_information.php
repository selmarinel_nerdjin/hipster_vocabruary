<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_information', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('user_id')->unique()->unsigned();
            $table->tinyInteger("age")->default(13);
            $table->date("birth")->nullable();
            $table->tinyInteger("gender")->default(0);
            $table->string("locale")->default("");
            $table->string("cover_id")->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
