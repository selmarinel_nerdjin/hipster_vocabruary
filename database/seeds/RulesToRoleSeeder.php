<?php

use Illuminate\Database\Seeder;

class RulesToRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** User */
        \App\Models\RuleToRole::query()->firstOrCreate([
            'role_id' => \App\Models\Roles::USER,
            'rule_id' => \App\Models\Rule::CAN_COMMENT
        ]);
        foreach ([\App\Models\Rule::CAN_COMMENT, \App\Models\Rule::CAN_ADD_WORD] as $rule) {
            \App\Models\RuleToRole::query()->firstOrCreate([
                'role_id' => \App\Models\Roles::VERIFIED_USER,
                'rule_id' => $rule
            ]);
        }
        foreach ([\App\Models\Rule::CAN_COMMENT,
                     \App\Models\Rule::CAN_ADD_WORD,
                     \App\Models\Rule::CAN_EDIT_WORD,
                     \App\Models\Rule::CAN_VERIFY] as $rule) {
            \App\Models\RuleToRole::query()->firstOrCreate([
                'role_id' => \App\Models\Roles::REPORTER,
                'rule_id' => $rule
            ]);
        }
        foreach ([\App\Models\Rule::CAN_COMMENT,
                     \App\Models\Rule::CAN_ADD_WORD,
                     \App\Models\Rule::CAN_EDIT_WORD,
                     \App\Models\Rule::CAN_VERIFY,
                     \App\Models\Rule::CAN_CREATE_FILTER,
                 ] as $rule) {
            \App\Models\RuleToRole::query()->firstOrCreate([
                'role_id' => \App\Models\Roles::MODERATOR,
                'rule_id' => $rule
            ]);
        }
        foreach ([\App\Models\Rule::CAN_COMMENT,
                     \App\Models\Rule::CAN_ADD_WORD,
                     \App\Models\Rule::CAN_EDIT_WORD,
                     \App\Models\Rule::CAN_VERIFY,
                     \App\Models\Rule::CAN_CREATE_FILTER,
                     \App\Models\Rule::CAN_REMOVE_FILTER,
                     \App\Models\Rule::CAN_REMOVE_COMMENT,
                 ] as $rule) {
            \App\Models\RuleToRole::query()->firstOrCreate([
                'role_id' => \App\Models\Roles::ADMIN,
                'rule_id' => $rule
            ]);
        }
        foreach (\App\Models\Rule::all()->map(function($model){
            return $model->id;
        }) as $rule) {
            \App\Models\RuleToRole::query()->firstOrCreate([
                'role_id' => \App\Models\Roles::SUPERUSER,
                'rule_id' => $rule
            ]);
        }

    }
}
