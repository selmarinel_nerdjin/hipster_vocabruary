<?php

use Illuminate\Database\Seeder;

class CreateSuperUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::query()->updateOrCreate([
            "email" => "selmarinel@gmail.com",
        ], [
            "name" => "su",
            "password" => Hash::make("1q2w3e4r")
        ]);
        \App\Models\RoleToUser::query()->firstOrCreate([
            "user_id" => $user->id,
            "role_id" => \App\Models\Roles::SUPERUSER
        ]);
    }
}
