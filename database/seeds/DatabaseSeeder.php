<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(init_roles::class);
//        $this->call(CreateSuperUser::class);
        $this->call(CreateRulesSeeder::class);
        $this->call(RulesToRoleSeeder::class);
    }
}
