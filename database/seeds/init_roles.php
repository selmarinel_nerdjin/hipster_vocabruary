<?php

use Illuminate\Database\Seeder;

class init_roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Roles::query()->firstOrCreate([
            "id" => \App\Models\Roles::USER,
            "name"=> "user"
        ]);
        \App\Models\Roles::query()->firstOrCreate([
            "id" => \App\Models\Roles::ADMIN,
            "name"=> "admin"
        ]);
        \App\Models\Roles::query()->firstOrCreate([
            "id" => \App\Models\Roles::SUPERUSER,
            "name"=> "su"
        ]);
        \App\Models\Roles::query()->firstOrCreate([
            "id" => \App\Models\Roles::MODERATOR,
            "name"=> "moderator"
        ]);
        \App\Models\Roles::query()->firstOrCreate([
            "id" => \App\Models\Roles::REPORTER,
            "name"=> "reporter"
        ]);
        \App\Models\Roles::query()->firstOrCreate([
            "id" => \App\Models\Roles::VERIFIED_USER,
            "name"=> "verified"
        ]);
    }
}
