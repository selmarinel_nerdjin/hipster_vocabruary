<?php

use Illuminate\Database\Seeder;

class CreateRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Rule::query()->firstOrCreate(["id" => 1], ['name' => "can_comment"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 2], ['name' => "can_add_word"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 3], ['name' => "can_edit_other_word"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 4], ['name' => "can_verify_word"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 5], ['name' => "can_remove_comment"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 6], ['name' => "can_create_filter"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 7], ['name' => "can_delete_filter"]);
        \App\Models\Rule::query()->firstOrCreate(["id" => 8], ['name' => "can_admin_user"]);
    }
}
