<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get("/page", "HomeController@page")->name("word:page");

Route::get("/api/user/permissions", "Admin\AdminController@getPermissions")->name("user:permissions");


Route::get("/facebook/login", "Auth\LoginController@facebookLogAction")->name("login:facebook");
Route::group(['prefix' => 'redirect'], function () {
    $asPrefix = "redirect";
    Route::get("/facebook", "Auth\LoginController@facebookAuthAction")->name("$asPrefix:facebook");
});

Route::get("/lang/{locale}", "Controller@setLocale")->name("lang");

Route::group(['prefix' => 'api'], function () {
    Route::post("/find", "HomeController@find")->name("api:find");
    Route::post("/get", "HomeController@get")->name("api:get");
    Route::post("/fb/login", "Auth\LoginController@fbLoginAction")->name("api:fb:login");
});

Route::group(['prefix' => 'word'], function () {
    $asPrefix = "word:";
    Route::post("/alphabet", "WordsController@loadAlphabet")->name($asPrefix . "alphabet");
    Route::post("/list", "WordsController@loadListAction")->name($asPrefix . "list");
    Route::post("/count", "WordsController@getCountAction")->name($asPrefix . 'count');
    Route::post("/get", "WordsController@getAction")->name($asPrefix . 'get');
    Route::post("/find", "WordsController@findAction")->name($asPrefix . 'find');
    Route::post("/random", "WordsController@getRandomAction")->name($asPrefix . 'random');
    Route::post("/popular", "WordsController@getPopularAction")->name($asPrefix . 'popular');
    Route::post("/newest", "WordsController@getNewestAction")->name($asPrefix . 'newest');
    Route::post("/like", "WordsController@likeAction")->name($asPrefix . 'like');
    Route::post("/rate", "WordsController@rateAction")->name($asPrefix . 'rate');
    Route::post("/dislike", "WordsController@dislikeAction")->name($asPrefix . 'dislike');
    Route::post("/comments", "WordsController@getCommentsAction")->name($asPrefix . 'comments');
    Route::put("/comment", "WordsController@putCommentAction")->name($asPrefix . 'comment');
    Route::post("/comment/delete", "WordsController@deleteCommentAction")->name($asPrefix . 'comment:delete');
});

Route::group(['prefix' => 'auth'], function () {
    $asPrefix = "auth";
//    Auth::routes();
    Route::post("login", "HomeController@loginAction")->name("$asPrefix:login");
    Route::post("logout", "HomeController@logoutAction")->name("$asPrefix:logout");
    Route::post("log", "Auth\LoginController@getUserAction")->name("$asPrefix:log");
    Route::post("in", "Auth\LoginController@login")->name("$asPrefix:in");
});

Route::group(['prefix' => 'side'], function () {
    Route::get("/users", "Panel\SideController@getUsers")->name("side:users")->middleware("su");
    Route::post("/api/users", "Panel\SideController@getUsersAPI")->name("side:users:api")->middleware("su");
    Route::post("/user", "Panel\SideController@getUser")->name("side:user");
    Route::put("/user", "Panel\SideController@updateUser")->name("side:user:update");
    Route::put("/user/verify", "Panel\SideController@verifyUser")->name("side:user:verify")->middleware("su");
    Route::put("/user/super", "Panel\SideController@superUser")->name("side:user:super")->middleware("su");
    Route::put("/user/ban", "Panel\SideController@banUser")->name("side:user:ban")->middleware("su");

    Route::put("/user/moder", "Panel\SideController@banUser")->name("side:user:moder")->middleware("su");
    Route::put("/user/report", "Panel\SideController@banUser")->name("side:user:report")->middleware("su");

    Route::put("/user/confirm", "Panel\SideController@confirmUser")->name("side:user:confirm")->middleware("su");

    Route::get("/api/filters", "Panel\SideController@getFilters")->name("side:filters:get");

    Route::get("/api/filter", "Panel\SideController@getFilter")->name("side:filter:get");
    Route::put("/api/filter", "Panel\SideController@putFilter")->name("side:filter:put")->middleware("su");
    Route::delete("/api/filter", "Panel\SideController@deleteFilter")->name("side:filter:delete")->middleware("su");
    Route::post("/api/filter", "Panel\SideController@createFilter")->name("side:filter:create")->middleware("su");

    Route::post("/api/security/auth", "Panel\SideController@securityAuth")->name("side:security:auth")->middleware("su");
});

Route::get('/', 'HomeController@landing')->name('landing');

Route::get('/{word?}', 'HomeController@index')->name('home');
