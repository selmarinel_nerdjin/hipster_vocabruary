<?php

Route::group(['prefix' => 'word'], function () {
    $asPrefix = "admin:";
    Route::post("/active", "AdminController@activeAction")->name($asPrefix . 'active');
});

Route::group(['prefix' => 'admin'], function () {
    $asPrefix = "admin:";
    Route::post("/create", "AdminController@createAction")->name($asPrefix . 'create');
    Route::get("/dashboard","AdminController@dashboard")->name("dashboard")->middleware('admin');
    Route::get("/dashboard/users","AdminController@usersAction")->name("dashboard:users")->middleware('admin');
    Route::put("/dashboard/user","AdminController@userSaveAction")->name("dashboard:user:put")->middleware('admin');
    Route::get("/dashboard/words","AdminController@wordsAction")->name("dashboard:words")->middleware('admin');
    Route::get("/dashboard/roles","AdminController@getRolesAction")->name("dashboard:roles:get")->middleware('admin');
});

